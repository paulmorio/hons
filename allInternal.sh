# Script to run all the external validation for all methods


#######################################################################################
################################# Per Style ICND ######################################
#######################################################################################

# # Fixed K MCMC Clustering
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/fixed_k_mcmc_10 
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/fixed_k_mcmc_10 
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/fixed_k_mcmc_weighted_10_ICND 
# we block out the final one because of the zero edge subgraph created in the final clustering. It is really cool though
# look up what we wrote in the report about it.
#python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/fixed_k_mcmc_weighted_10_ICNP  

# # Variable K MCMC Clustering
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt cleanClusters1
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt cleanClusters1
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt cleanClusters2 
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt cleanClusters3  

# K Clique Percolation
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/cliq_uw_K3  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/cliq_uw_K3
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/cliq_w_K3_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/cliq_w_K3_ICNP  

# MCODE
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/mcode_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/mcode_uw
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/mcode_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/mcode_w_ICNP  

# DPClus
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/dpclus_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/dpclus_uw
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/dpclus_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/dpclus_w_ICNP  

# IPCA
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/ipca_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/ipca_uw 
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/ipca_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/ipca_w_ICNP  


# Graph Entropy
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/graph_entropy_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/graph_entropy_uw 
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/graph_entropy_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/graph_entropy_w_ICNP  

# CoAch
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/coach_uw
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/coach_w_ICNP  