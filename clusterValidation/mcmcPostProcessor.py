"""
Cleans away all no density clusters to create clean clusterings from dirty ones
"""


import sys
import networkx as nx

arg = sys.argv
dataFile = arg[1]
clusteringFile = arg[2]

graph = nx.read_weighted_edgelist(dataFile)

# dictionry of the clusterings predicted
clusterList = []
f = open(clusteringFile, 'r')
for line in f: 
	line.strip('\n')
	cluster = line.split()
	clusterList.append(cluster)
f.close()

cleanClusters = []
for cluster in clusterList:
	subgraph_x = graph.subgraph(cluster)
	if subgraph_x.number_of_edges() == 0:
		continue
	cleanClusters.append(subgraph_x)

for cluster in cleanClusters:
	print ' '.join(cluster)