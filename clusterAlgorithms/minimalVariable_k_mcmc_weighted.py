# Author Paul Scherer
# Based on algorithm by Dr. Tom Thorne
import sys
import networkx as nx
import numpy as np
import random
from collections import defaultdict, deque
from itertools import combinations

arg = sys.argv
infile = arg[1]		# data or the network
k = int(arg[2])
alpha = int(arg[3])
beta = int(arg[4])
steps = int(arg[5])

gamma = 1

# we start by reading in the network
graph = nx.read_weighted_edgelist(infile)

# grab the weights so that we can multiply them to the edges
edge_weights = nx.get_edge_attributes(graph,'weight')

# The initial possible cluster assignments as a li
indicators = []
for i in range(0,k):
	indicators.append(i)

# Set initial clusterIndicator list to random integers in range 1...K
clusterIndicators = {}
for x in graph.nodes():
	clusterIndicators[x] = random.randint(0,len(indicators)-1)

# Initialize the symmetric matrix w with each initial element being a random
# variable distributed as a Beta(alpha, beta)
w = defaultdict(defaultdict)
for i in range(0,len(indicators)):
	for j in range(0, len(indicators)):
		initialValue = np.random.beta(alpha, beta)
		w[i][j] = initialValue
		w[j][i] = initialValue



def normalise(vector):
	norm = vector/np.sum(vector)
	#norm = [float(i)/sum(vector) for i in vector]
	return norm

# WEIGHTED CHANGE COMES FROM HERE NEIGHBOURS ARE WEIGHTED
def proteinClusterProb(protein, membership, clusterIndicators, weights, network, edge_weights, num):
	"""
	returns $$ p(z_i = k | w, A) = 
	\prod_{j \in neighbours(i)}w_{k,z_j} \prod_{j notin neighbours(i)}(1-w_{k,z_j}) $$
	"""
	# Normal implementation THIS UNDEFLOWS CAUSE PYTHON IS WEAK
	# so we use logs and hacks
	firstProdList = []
	for neighbour in network.neighbors(protein):
		weightedEdge = 1.0
		if ((protein, neighbour) in edge_weights):
			weightedEdge = edge_weights[(protein, neighbour)]
		if ((neighbour, protein) in edge_weights):
			weightedEdge = edge_weights[(neighbour, protein)]
		firstProdList.append(np.log(weights[membership][clusterIndicators[neighbour]]))
	firstProd = (np.sum(firstProdList))
	
	secondProdList = []
	for nonNeighbour in range(0,num):
		secondProdList.append(np.log(1-weights[membership][nonNeighbour]))
	secondProd = (np.sum(secondProdList))
	
	return np.sum([firstProd, secondProd])

def clusterPrior(k, m_count_noi, network):
	"""
	calculates p(z_i = k | z_-i, gamma)
	the prior probability of clusters
	"""
	prior_prob = (m_count_noi[k])/float(nx.number_of_nodes(network)+gamma-1) 
	return prior_prob 

def newClusterPrior(network, gamma):
	"""
	The prior for a new cluster is 
	p(z_i = K+1 | z_i, gamma) = gamma / (n + gamma -1)
	where n is the number of nodes in the network
	"""
	numNodes = nx.number_of_nodes(network)
	new_cluster_prior = float(gamma) / (numNodes + gamma - 1)
	return new_cluster_prior


def cool_smoother(r):
	"""
	smoothes the negative values in the r while preserving ratios with an epsilon
	this is very similar to laplace smoothing
	"""
	epsilon = 0.001
	biggest = abs(min(r)) + epsilon
	smoothed = r
	for i in range(0,len(r)):
		smoothed[i] += biggest

	return smoothed

def clusterIndicatorCount(k, clusterIndicators):
	"""
	Returns the number of proteins classified as k in clusterIndicators
	"""
	count = 0
	for protein in clusterIndicators.keys():
		if clusterIndicators[protein] == k:
			count += 1

	return count

def clusterIndicatorMatrixCount(clusterIndicators, indicators, network):
	"""
	calculate n_k,l which is a symmetric matrix (of sorts, here it is a hash)
	which contains the number of interactions from zi = k to zj = l
	"""
	counts = 0
	countsDict = defaultdict(defaultdict)	
	for i in range(0,len(indicators)):
		for j in range(0,len(indicators)):
			countsDict[i][j] = 0
			countsDict[j][i] = 0

	for prot_a, prot_b in network.edges():
		a_indicator = clusterIndicators[prot_a]
		b_indicator = clusterIndicators[prot_b]
		countsDict[a_indicator][b_indicator] += 1
		if a_indicator != b_indicator:
			countsDict[b_indicator][a_indicator] += 1

	return countsDict


# Main loop (Gibbs Algorithm)
for step in range(0,steps):
	print step
	count = 0
	for protein in graph.nodes():
		
		# calculate m_k, -i
		m_count_noi = []
		for i in indicators:
			m_count_noi.append(clusterIndicatorCount(i, clusterIndicators) - 1) 

		# create vector r of length K and store the posterior in it
		r = []
		for i in range(0,len(indicators)):

			likelihood = proteinClusterProb(protein, i, clusterIndicators, w, graph, edge_weights, k)
			prior = clusterPrior(i, m_count_noi, graph)
			r.append(likelihood*prior)

		#print w.keys(), w[0].keys(), "before"

		# Generate a random extra/column appended to w using random beta(a,b)
		from copy import deepcopy
		temp_w = deepcopy(w)
		len_indicators = len(indicators)
		k = len_indicators
		# for each existing row we add a column of randoms
		for row in temp_w.keys():
			temp_w[row][len_indicators] = np.random.beta(alpha, beta)

		# add a new row
		for i in range(0,len_indicators+1):
			temp_w[len_indicators][i] = np.random.beta(alpha, beta)


		#calculate posterior for k + 1
		newCluster_likelihood = proteinClusterProb(protein, len_indicators, clusterIndicators, temp_w, graph, edge_weights, k)
		newCluster_prior = newClusterPrior(graph, gamma)
		product = newCluster_likelihood * newCluster_prior
		r.append(product)
		
		# we have to smooth the values in r because their are negatives from the log
		# computations and taking the absolute value doesnt preserve ratios
		# we normalise r to sum to 1 so that we may use the weights
		# and sample a new assignment for the protein as random integer
		# in the range 1...K with 
		r = cool_smoother(r)
		r = normalise(r)
		temp_indicators = deepcopy(indicators)
		temp_indicators.append(len(indicators))

		new_assignment = np.random.choice(temp_indicators, p=r)
		clusterIndicators[protein] = new_assignment

		# decision to keep new cluster or not based on whether the protein has
		# been assigned to it
		# With this we favor highly connected nodes to stick together than not
		if new_assignment == len_indicators:
			w = deepcopy(temp_w)
			indicators = deepcopy(temp_indicators)

		print protein, count
		count += 1

	# calculate the number m_k which is the number of proteins that equal k in 1..K
	m_count = []
	for i in indicators:
		m_count.append(clusterIndicatorCount(i, clusterIndicators))

	# for k = K to 1 (well 0 because we are 0 labeling computer scientists of sorts)
	len_indicators = len(indicators)
	for i in range(len_indicators-1,-1,-1):
		if m_count[i] == 0:
			# reasign weights to lower rows and columns
			for j in range(i+1,len_indicators):
				for l in range(i+1,len_indicators):
					w[j-1][l-1] = w[j][l]
					w[l-1][j-1] = w[l][j]

				#update cluster indicators
				for protein in graph.nodes():
					if clusterIndicators[protein] == j: 
						clusterIndicators[protein] -= 1

			# delete maximum row/column
			if (len_indicators) in w: del w[len_indicators]
			# remove max class from indicators
			indicators.remove(max(indicators))

	# calculate n_k,l which is a symmetric matrix (of sorts, here it is a hash)
	# which contains the number of interactions from zi = k to zj = l
	countsDict = clusterIndicatorMatrixCount(clusterIndicators, indicators, graph)

	# updating of the weight matrix
	for i in range(0,len(indicators)-1):
		for j in range(i,len(indicators)):
			new_w_value = np.random.beta(alpha + countsDict[i][j], beta + (m_count[i]*m_count[j] - countsDict[i][j]))
			w[i][j] = new_w_value
			w[j][i] = new_w_value

# having performed the loop we store the clusterings for spitting out later
cluster_members_dict = defaultdict(list)
for i in indicators:
	for protein in clusterIndicators.keys():
		if clusterIndicators[protein] == i:
			cluster_members_dict[i].append(protein)

for i in indicators:
	# we leave out clusters with only one element cause they are useless
	if len(cluster_members_dict[i]) > 1:
		print ' '.join(cluster_members_dict[i])