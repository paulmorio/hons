# Based on algorithm by Dr. Tom Thorne
import sys
import networkx as nx
import numpy as np
import random
from collections import defaultdict, deque
from itertools import combinations

arg = sys.argv
infile = arg[1]		# data or the network
K = int(arg[2])
alpha = int(arg[3])
beta = int(arg[4])
steps = int(arg[5])

# we start by reading in the network
graph = nx.read_weighted_edgelist(infile)

# grab the weights so that we can multiply them to the edges
edge_weights = nx.get_edge_attributes(graph,'weight')

# The possible cluster assignments as a list
indicators = []
for i in range(0,K):
	indicators.append(i)

# Set initial clusterIndicator list to random integers in range 1...K
clusterIndicators = {}
for x in graph.nodes():
	clusterIndicators[x] = random.randint(0,K-1)

# Initialize the symmetric matrix w with each initial element being a random
# variable distributed as a Beta(alpha, beta)
w = defaultdict(defaultdict)
for i in range(0,K):
	for j in range(0, K):
		initialValue = np.random.beta(alpha, beta)
		w[i][j] = initialValue
		w[j][i] = initialValue

def normalise(vector):
	norm = vector/np.sum(vector)
	#norm = [float(i)/sum(vector) for i in vector]
	return norm

# WEIGHTED CHANGE COMES FROM HERE NEIGHBOURS ARE WEIGHTED
def proteinClusterProb(protein, membership, clusterIndicators, weights, network, edge_weights):
	"""
	returns $$ p(z_i = k | w, A) = 
	\prod_{j \in neighbours(i)}w_{k,z_j} \prod_{j notin neighbours(i)}(1-w_{k,z_j}) $$
	"""
	# Normal implementation THIS UNDEFLOWS CAUSE PYTHON IS WEAK
	# so we use logs and hacks
	firstProdList = []
	for neighbour in network.neighbors(protein):
		weightedEdge = 1.0
		if ((protein, neighbour) in edge_weights):
			weightedEdge = edge_weights[(protein, neighbour)]
		if ((neighbour, protein) in edge_weights):
			weightedEdge = edge_weights[(neighbour, protein)]
		firstProdList.append(np.log(weights[membership][clusterIndicators[neighbour]]))
	firstProd = (np.sum(firstProdList))
	
	secondProdList = []
	for nonNeighbour in nx.non_neighbors(graph,protein):
		secondProdList.append(np.log(1-weights[membership][clusterIndicators[nonNeighbour]]))
	secondProd = (np.sum(secondProdList))
	
	return np.sum([firstProd, secondProd])

def cool_smoother(r):
	"""
	smoothes the negative values in the r while preserving ratios with an epsilon
	this is very similar to laplace smoothing
	"""
	epsilon = 0.001
	biggest = abs(min(r)) + epsilon
	smoothed = r
	for i in range(0,len(r)):
		smoothed[i] += biggest

	return smoothed

def clusterIndicatorCount(k, clusterIndicators):
	"""
	Returns the number of proteins classified as k in clusterIndicators
	"""
	count = 0
	for protein in clusterIndicators.keys():
		if clusterIndicators[protein] == k:
			count += 1

	return count

def clusterIndicatorMatrixCount(clusterIndicators, indicators, network):
	"""
	calculate n_k,l which is a symmetric matrix (of sorts, here it is a hash)
	which contains the number of interactions from zi = k to zj = l
	"""
	counts = 0
	countsDict = defaultdict(defaultdict)	
	for i in range(0,len(indicators)):
		for j in range(0,len(indicators)):
			countsDict[i][j] = 0
			countsDict[j][i] = 0

	for prot_a, prot_b in network.edges():
		a_indicator = clusterIndicators[prot_a]
		b_indicator = clusterIndicators[prot_b]
		countsDict[a_indicator][b_indicator] += 1
		if a_indicator != b_indicator:
			countsDict[b_indicator][a_indicator] += 1

	return countsDict

# Main loop (Gibbs Algorithm)
steps = 10
for step in range(0,steps):
	print step
	counting = 0
	for protein in graph.nodes():
		# create vector r of length K
		r = []
		for i in range(0,len(indicators)):
			r.append(proteinClusterProb(protein, i, clusterIndicators, w, graph, edge_weights))

		# we have to smooth the values in r because their are negatives from the log
		# computations and taking the absolute value doesnt preserve ratios
		# we normalise r to sum to 1 so that we may use the weights
		# and sample a new assignment for the protein as random integer
		# in the range 1...K with 
		r = cool_smoother(r)
		r = normalise(r)
		new_assignment = np.random.choice(indicators, p=r)
		clusterIndicators[protein] = new_assignment

	# calculate the number m_k which is the number of proteins that equal k in 1..K
	m_count = []
	for i in indicators:
		m_count.append(clusterIndicatorCount(i, clusterIndicators))
	
	# calculate n_k,l which is a symmetric matrix (of sorts, here it is a hash)
	# which contains the number of interactions from zi = k to zj = l
	countsDict = clusterIndicatorMatrixCount(clusterIndicators, indicators, graph)

	# updating of the weight matrix
	for i in range(0,len(indicators)-1):
		for j in range(i,len(indicators)):
			new_w_value = np.random.beta(alpha + countsDict[i][j], beta + (m_count[i]*m_count[j] - countsDict[i][j]))
			w[i][j] = new_w_value
			w[j][i] = new_w_value

# having performed the loop we store the clusterings for spitting out later
cluster_members_dict = defaultdict(list)
for i in indicators:
	for protein in clusterIndicators.keys():
		if clusterIndicators[protein] == i:
			cluster_members_dict[i].append(protein)

for i in indicators:
	# we leave out clusters with only one element cause they are useless
	if len(cluster_members_dict[i]) > 1:
		print ' '.join(cluster_members_dict[i])