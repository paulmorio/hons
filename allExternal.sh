# Script to run all the external validation for all methods


#######################################################################################
########################## Per Style (weight or not) ##################################
#######################################################################################

#### Unweighted
# # Fixed K MCMC Clustering
# python externalValidation.py clusterValidation/fixed_k_mcmc_10 data/ground_truth.txt 
# # Variable K MCMC Clustering

# # K Clique Percolation
# python externalValidation.py clusterValidation/cliq_uw_K3 data/ground_truth.txt 
# # MCODE
# python externalValidation.py clusterValidation/mcode_uw data/ground_truth.txt 
# # DPClus
# python externalValidation.py clusterValidation/dpclus_uw data/ground_truth.txt 
# # IPCA
# python externalValidation.py clusterValidation/ipca_uw data/ground_truth.txt 
# # Graph Entropy
# python externalValidation.py clusterValidation/graph_entropy_uw data/ground_truth.txt 
# # CoAch
# python externalValidation.py clusterValidation/coach_uw data/ground_truth.txt 



#### Weighted ICND
# # Fixed K MCMC Clustering
# python externalValidation.py clusterValidation/fixed_k_mcmc_weighted_10_ICND data/ground_truth.txt 
# # Variable K MCMC Clustering

# # K Clique Percolation
# python externalValidation.py clusterValidation/cliq_w_K3_ICND data/ground_truth.txt 
# # MCODE
# python externalValidation.py clusterValidation/mcode_w_ICND data/ground_truth.txt 
# # DPClus
# python externalValidation.py clusterValidation/dpclus_w_ICND data/ground_truth.txt 
# # IPCA
# python externalValidation.py clusterValidation/ipca_w_ICND data/ground_truth.txt 
# # Graph Entropy
# python externalValidation.py clusterValidation/graph_entropy_w_ICND data/ground_truth.txt 
# # CoAch
# python externalValidation.py clusterValidation/coach_w_ICND data/ground_truth.txt 




#### Weighted ICNP
# # Fixed K MCMC Clustering
# python externalValidation.py clusterValidation/fixed_k_mcmc_weighted_10_ICNP data/ground_truth.txt 
# # Variable K MCMC Clustering

# # K Clique Percolation
# python externalValidation.py clusterValidation/cliq_w_K3_ICNP data/ground_truth.txt 
# # MCODE
# python externalValidation.py clusterValidation/mcode_w_ICNP data/ground_truth.txt 
# # DPClus
# python externalValidation.py clusterValidation/dpclus_w_ICNP data/ground_truth.txt 
# # IPCA
# python externalValidation.py clusterValidation/ipca_w_ICNP data/ground_truth.txt 
# # Graph Entropy
# python externalValidation.py clusterValidation/graph_entropy_w_ICNP data/ground_truth.txt 
# # CoAch
# python externalValidation.py clusterValidation/coach_w_ICNP data/ground_truth.txt 



#######################################################################################
################################### Per Method ########################################
#######################################################################################

# # Fixed K MCMC Clustering
python externalValidation.py clusterValidation/fixed_k_mcmc_10 data/ground_truth.txt
python externalValidation.py clusterValidation/fixed_k_mcmc_weighted_10_ICND data/ground_truth.txt
python externalValidation.py clusterValidation/fixed_k_mcmc_weighted_10_ICNP data/ground_truth.txt 

# # Variable K MCMC Clustering
python externalValidation.py cleanClusters1 data/ground_truth.txt
python externalValidation.py cleanClusters2 data/ground_truth.txt
python externalValidation.py cleanClusters3 data/ground_truth.txt 

# K Clique Percolation
python externalValidation.py clusterValidation/cliq_uw_K3 data/ground_truth.txt 
python externalValidation.py clusterValidation/cliq_w_K3_ICND data/ground_truth.txt 
python externalValidation.py clusterValidation/cliq_w_K3_ICNP data/ground_truth.txt 

# MCODE
python externalValidation.py clusterValidation/mcode_uw data/ground_truth.txt 
python externalValidation.py clusterValidation/mcode_w_ICND data/ground_truth.txt 
python externalValidation.py clusterValidation/mcode_w_ICNP data/ground_truth.txt 

# DPClus
python externalValidation.py clusterValidation/dpclus_uw data/ground_truth.txt 
python externalValidation.py clusterValidation/dpclus_w_ICND data/ground_truth.txt 
python externalValidation.py clusterValidation/dpclus_w_ICNP data/ground_truth.txt 

# IPCA
python externalValidation.py clusterValidation/ipca_uw data/ground_truth.txt 
python externalValidation.py clusterValidation/ipca_w_ICND data/ground_truth.txt 
python externalValidation.py clusterValidation/ipca_w_ICNP data/ground_truth.txt 


# Graph Entropy
python externalValidation.py clusterValidation/graph_entropy_uw data/ground_truth.txt 
python externalValidation.py clusterValidation/graph_entropy_w_ICND data/ground_truth.txt 
python externalValidation.py clusterValidation/graph_entropy_w_ICNP data/ground_truth.txt 

# CoAch
python externalValidation.py clusterValidation/coach_uw data/ground_truth.txt 
python externalValidation.py clusterValidation/coach_w_ICND data/ground_truth.txt 
python externalValidation.py clusterValidation/coach_w_ICNP data/ground_truth.txt 
