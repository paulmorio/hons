# Script to run all the external validation for all methods


#######################################################################################
################################# Per Style ICND ######################################
#######################################################################################

# # Fixed K MCMC Clustering
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/fixed_k_mcmc_10 drawings/hiveplots/fixed_k_mcmc_hive_uw_nodes drawings/hiveplots/fixed_k_mcmc_hive_uw_links
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/fixed_k_mcmc_weighted_10_ICND drawings/hiveplots/fixed_k_mcmc_hive_icnd_nodes drawings/hiveplots/fixed_k_mcmc_hive_icnd_links
python hive_plot_dataConverter.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/fixed_k_mcmc_weighted_10_ICNP drawings/hiveplots/fixed_k_mcmc_hive_icnp_nodes drawings/hiveplots/fixed_k_mcmc_hive_icnp_links

# # Variable K MCMC Clustering
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/variable_k_mcmc_uw_clean drawings/hiveplots/variable_k_mcmc_hive_uw_nodes drawings/hiveplots/variable_k_mcmc_hive_uw_links
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/variable_k_mcmc_w_icnd_clean drawings/hiveplots/variable_k_mcmc_hive_icnd_nodes drawings/hiveplots/variable_k_mcmc_hive_icnd_links
python hive_plot_dataConverter.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/variable_k_mcmc_w_icnp_clean drawings/hiveplots/variable_k_mcmc_hive_icnp_nodes drawings/hiveplots/variable_k_mcmc_hive_icnp_links

# K Clique Percolation
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/cliq_uw_K3 drawings/hiveplots/cliq_uw_nodes drawings/hiveplots/cliq_uw_links
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/cliq_w_K3_ICND drawings/hiveplots/cliq_icnd_nodes drawings/hiveplots/cliq_icnd_links
python hive_plot_dataConverter.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/cliq_w_K3_ICNP drawings/hiveplots/cliq_icnp_nodes drawings/hiveplots/cliq_icnp_links

# MCODE
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/mcode_uw drawings/hiveplots/mcode_uw_nodes drawings/hiveplots/mcode_uw_links
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/mcode_w_ICND drawings/hiveplots/mcode_icnd_nodes drawings/hiveplots/mcode_icnd_links
python hive_plot_dataConverter.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/mcode_w_ICNP drawings/hiveplots/mcode_icnp_nodes drawings/hiveplots/mcode_icnp_links

# DPClus
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/dpclus_uw drawings/hiveplots/dpclus_uw_nodes drawings/hiveplots/dpclus_uw_links
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/dpclus_w_ICND drawings/hiveplots/dpclus_icnd_nodes drawings/hiveplots/dpclus_icnd_links
python hive_plot_dataConverter.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/dpclus_w_ICNP drawings/hiveplots/dpclus_w_icnd_nodes drawings/hiveplots/dpclus_w_icnd_nlinks

# IPCA
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/ipca_uw drawings/hiveplots/ipca_uw_nodes drawings/hiveplots/ipca_uw_links
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/ipca_w_ICND drawings/hiveplots/ipca_icnd_nodes drawings/hiveplots/ipca_icnd_links
python hive_plot_dataConverter.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/ipca_w_ICNP drawings/hiveplots/ipca_icnp_nodes drawings/hiveplots/ipca_icnp_links


# Graph Entropy
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/graph_entropy_uw drawings/hiveplots/ge_uw_nodes drawings/hiveplots/ge_uw_links
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/graph_entropy_w_ICND drawings/hiveplots/ge_icnd_nodes drawings/hiveplots/ge_icnd_links
python hive_plot_dataConverter.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/graph_entropy_w_ICNP drawings/hiveplots/ge_icnp_nodes drawings/hiveplots/ge_icnp_links

# CoAch
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_uw drawings/hiveplots/coach_uw_nodes drawings/hiveplots/coach_uw_links  
python hive_plot_dataConverter.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_w_ICND drawings/hiveplots/coach_icnd_nodes drawings/hiveplots/coach_icnd_links 
python hive_plot_dataConverter.py data/network_ICNP_1_wo_zero_weights.txt clusterValidation/coach_w_ICNP drawings/hiveplots/coach_icnp_nodes drawings/hiveplots/coach_icnp_links 