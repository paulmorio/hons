#README for Honours Project

## Requirements 
It is necessary to install the requirements on a `virtualenvironment` running the latest release of 2.7.x (it should work on any 2.7.x theoretically).

Installing the requirements (not all necessary, but for the sake of simplicity) can be done via this command. (inside the activated virtual environment)

"bash install-dependencies.sh"

## PLEASE READ
Many of the scripts can take an immense amount of time to run when MCMC methods are concerned so please
make sure to read the scripts before running them. In fact it is likely not necessary as all results have
been added in the clusterValidation folder.

Looking inside them should reveal usage of each file.

## Quick Description of Folders
- clusterAlgorithms contains all the python implementations of the clustering methods.
- clusterValidation contains all results of the clusterings as well as postprocessing files where necessary
- data contains the network data, unweighted, ICND, ICNP, with or without zero weight edges, and the ground truth set
- drawings contain sample drawing made as well as old examples
- fastsemsim is the python package for quickly finding semantic similarity measures
- legendFolder is a folder for side effects created with the new modified version of Dr. Thorne's drawing algorithm
- notebooks contains some older ipython notebooks used to practice techniques and create initial visualisations
- plot contains Dr. Thorne's original and modified versions of the drawing algorithm made in the project
- support contains many old files that were used to create initial validations
- yamlFiles is a folder containing .yaml files created from modBundle.p or Bundle.py scripts in drawing algorithms.


