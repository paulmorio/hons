# This script automates the graphing process of all available clusterings
# In this current string we made drawings for all the ICND weighted methods 

#### Normal ModBundle Edition ####
# # Fixed K MCMC Clustering
# python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_w_ICND yamlFiles/coachICND.yaml 0.001 legendFolder/coachLegend
# python plot/modDraw.py yamlFiles/coachICND.yaml clusterValidation/coach_w_ICND coachICND.pdf legendFolder/coachLegend

# # Variable K MCMC Clustering
# python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_w_ICND yamlFiles/coachICND.yaml 0.001 legendFolder/coachLegend
# python plot/modDraw.py yamlFiles/coachICND.yaml clusterValidation/coach_w_ICND coachICND.pdf legendFolder/coachLegend

# K Clique Percolation
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/cliq_w_K3_ICND yamlFiles/cliqICND.yaml 0.001 legendFolder/cliqLegend
python plot/modDraw.py yamlFiles/cliqICND.yaml clusterValidation/cliq_w_K3_ICND cliqICNDColor.pdf legendFolder/cliqLegend

# MCODE
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend
python plot/modDraw.py yamlFiles/mcodeICND.yaml clusterValidation/mcode_w_ICND mcodeICNDColor.pdf legendFolder/mcodeLegend

# DPClus
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/dpclus_w_ICND yamlFiles/dpClusICND.yaml 0.001 legendFolder/dpclusLegend
python plot/modDraw.py yamlFiles/dpClusICND.yaml clusterValidation/dpclus_w_ICND dpClusICNDColor.pdf legendFolder/dpclusLegend

# IPCA
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/ipca_w_ICND yamlFiles/ipcaICND.yaml 0.001 legendFolder/ipcaLegend
python plot/modDraw.py yamlFiles/ipcaICND.yaml clusterValidation/ipca_w_ICND ipcaICNDColor.pdf legendFolder/ipcaLegend

# Graph Entropy
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/graph_entropy_w_ICND yamlFiles/graphEntropyICND.yaml 0.001 legendFolder/graphEntropyLegend
python plot/modDraw.py yamlFiles/graphEntropyICND.yaml clusterValidation/graph_entropy_w_ICND graphEntropyICNDColor.pdf legendFolder/graphEntropyLegend

# CoAch
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_w_ICND yamlFiles/coachICND.yaml 0.001 legendFolder/coachLegend
python plot/modDraw.py yamlFiles/coachICND.yaml clusterValidation/coach_w_ICND coachICNDColor.pdf legendFolder/coachLegend


#### Normal with only cluster edges ####
# Fixed K MCMC Clustering

# Variable K MCMC Clustering


# # K Clique Percolation
# python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/cliq_w_K3_ICND yamlFiles/cliqICND.yaml 0.001 legendFolder/cliqLegend 241
# python plot/modDraw.py yamlFiles/cliqICND.yaml clusterValidation/cliq_w_K3_ICND cliqICNDcedge.pdf legendFolder/cliqLegend

# MCODE
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend 81
python plot/modDraw.py yamlFiles/mcodeICND.yaml clusterValidation/mcode_w_ICND mcodeICNDcedgeColor.pdf legendFolder/mcodeLegend

# DPClus
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/dpclus_w_ICND yamlFiles/dpClusICND.yaml 0.001 legendFolder/dpclusLegend 713
python plot/modDraw.py yamlFiles/dpClusICND.yaml clusterValidation/dpclus_w_ICND dpClusICNDcedgeColor.pdf legendFolder/dpclusLegend

# IPCA
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/ipca_w_ICND yamlFiles/ipcaICND.yaml 0.001 legendFolder/ipcaLegend 1420
python plot/modDraw.py yamlFiles/ipcaICND.yaml clusterValidation/ipca_w_ICND ipcaICNDcedgeColor.pdf legendFolder/ipcaLegend

# Graph Entropy
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/graph_entropy_w_ICND yamlFiles/graphEntropyICND.yaml 0.001 legendFolder/graphEntropyLegend 644
python plot/modDraw.py yamlFiles/graphEntropyICND.yaml clusterValidation/graph_entropy_w_ICND graphEntropyICNDcedgeColor.pdf legendFolder/graphEntropyLegend

# CoAch
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_w_ICND yamlFiles/coachICND.yaml 0.001 legendFolder/coachLegend 1730
python plot/modDraw.py yamlFiles/coachICND.yaml clusterValidation/coach_w_ICND coachICNDcedgeColor.pdf legendFolder/coachLegend


#### Subgraphs of Clustered Nodes ####
# Fixed K MCMC Clustering

# Variable K MCMC Clustering

# K Clique Percolation
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/cliq_w_K3_ICND yamlFiles/cliqICND.yaml 0.001 legendFolder/cliqLegend
python plot/modDraw.py yamlFiles/cliqICND.yaml clusterValidation/cliq_w_K3_ICND cliqICNDsubColor.pdf legendFolder/cliqLegend

# MCODE
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend
python plot/modDraw.py yamlFiles/mcodeICND.yaml clusterValidation/mcode_w_ICND mcodeICNDsubColor.pdf legendFolder/mcodeLegend

# DPClus
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/dpclus_w_ICND yamlFiles/dpClusICND.yaml 0.001 legendFolder/dpclusLegend
python plot/modDraw.py yamlFiles/dpClusICND.yaml clusterValidation/dpclus_w_ICND dpClusICNDsubColor.pdf legendFolder/dpclusLegend

# IPCA
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/ipca_w_ICND yamlFiles/ipcaICND.yaml 0.001 legendFolder/ipcaLegend
python plot/modDraw.py yamlFiles/ipcaICND.yaml clusterValidation/ipca_w_ICND ipcaICNDsubColor.pdf legendFolder/ipcaLegend

# Graph Entropy
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/graph_entropy_w_ICND yamlFiles/graphEntropyICND.yaml 0.001 legendFolder/graphEntropyLegend
python plot/modDraw.py yamlFiles/graphEntropyICND.yaml clusterValidation/graph_entropy_w_ICND graphEntropyICNDsubColor.pdf legendFolder/graphEntropyLegend

# CoAch
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt clusterValidation/coach_w_ICND yamlFiles/coachICND.yaml 0.001 legendFolder/coachLegend
python plot/modDraw.py yamlFiles/coachICND.yaml clusterValidation/coach_w_ICND coachICNDsubColor.pdf legendFolder/coachLegend


