public class Distance1 {

	public static void main(String args[]){
		int a,b,min,max;
		a = Integer.parseInt(args[0]);
		b = Integer.parseInt(args[1]);
		min = Math.min(a,b);
		max = Math.max(a,b);
		int c = max-min;

		System.out.println(c);
	}
}