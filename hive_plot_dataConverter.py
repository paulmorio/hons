"""
This module takes a clustering file and produces an output node and link file that can be
read by the wodak labs hive plot implementation.

See usage tab of their website for notes of the format that they need that is provided here

Author: Paul Scherer
"""
import sys
import random
import networkx as nx

arg = sys.argv
dataFile = arg[1]
clusteringFile = arg[2]
node_output = arg[3]
interactions_output = arg[4]

graph = nx.read_weighted_edgelist(dataFile)

# dictionary of the clusterings predicted
clusterList = []
f = open(clusteringFile, 'r')
for line in f: 
	line.strip('\n')
	cluster = line.split()
	clusterList.append(cluster)
f.close()

# Color picking for the clusters
# We create a dictionary of colors
colorDict = {}
colorCounter = 1
for cluster in clusterList:
	r = random.randint(0,255)
	g = random.randint(0,255)
	b = random.randint(0,255)
	colorDict[colorCounter] = (r,g,b)
	colorCounter += 1

# set nodes and write to file
f = open(node_output,'w')
f.write("id\taxis\tlabel\tvalue\theight\tr\tg\tb\ta\n")
clusterCounter = 0
for cluster in clusterList:
	clusterCounter += 1
	r,g,b = colorDict[clusterCounter]
	memberCounter = 0
	for member in cluster:
		memberCounter += 1
		f.write(member + "\t" + str(clusterCounter) + "a" + "\t" + str(clusterCounter) + member + "\t" + str(memberCounter) + "\t" + "1" + "\t"+ str(r) + "\t" + str(g) + "\t" + str(b) + "\t" + str(200) + "\n")
f.close()

# Evil next level list comprehension skills
purelyNodes = [j for i in clusterList for j in i]
subgraph = graph.subgraph(purelyNodes)

# Writing down the interactions
f = open(interactions_output, 'w')
f.write("a\tb")
for edge in subgraph.edges():
	a,b = edge
	f.write(a + "\t" + b + "\n")
f.close()