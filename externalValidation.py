# # Author: Paul Scherer (paulmorio)
# This module covers the external validation of clusterings as described in the report

import sys
import numpy as np
import networkx as nx
from collections import defaultdict
from collections import Counter

arg = sys.argv
clusteringFile = arg[1]
ground_truth = arg[2]

# dictionry of the clusterings predicted
clusterDict = {}
f = open(clusteringFile, 'r')
clusterCount = 0
for line in f: 
	line.strip('\n')
	cluster = line.split()
	clusterDict[clusterCount] = cluster
	clusterCount += 1
f.close()

# dictionary
groundTruth_dict = {}
f = open(ground_truth, 'r')
clusterCount = 0
for line in f: 
	line.strip('\n')
	cluster = line.split()
	groundTruth_dict[clusterCount] = cluster
	clusterCount += 1
f.close()

def fScore(c1, c2):
	"""
	Calculates the f-score between two clusters
	"""
	intersect = [val for val in c1 if val in c2]
	recall = len(intersect)/float(len(c2))
	precision = len(intersect)/float(len(c1))
	if (recall == 0 and precision == 0):
		return 0
	
	f_score = float(2*recall*precision)/(recall+precision)
	return f_score

def fowlesMallows(c1,c2):
	"""
	Calculates the fowlesMallows-score between two clusters
	"""
	intersect = [val for val in c1 if val in c2]
	recall = len(intersect)/float(len(c2))
	precision = len(intersect)/float(len(c1))
	if (recall == 0 and precision == 0):
		return 0
	
	fowlsScore = np.sqrt(recall*precision)
	return fowlsScore


# for each of the predicted complexes we have compare them to each of the ground truth complexes
# via highest f-score statistic.
f_score_list = []
fowlesMallows_list = []
for pred_complex in clusterDict.keys():
	temp_list = []
	temp_list2 = []
	for ground_truth_complex in groundTruth_dict.keys():
		temp_list.append(fScore(clusterDict[pred_complex], groundTruth_dict[ground_truth_complex]))
		temp_list2.append(fowlesMallows(clusterDict[pred_complex],groundTruth_dict[ground_truth_complex]))
	f_score_list.append(max(temp_list))
	fowlesMallows_list.append(max(temp_list2))


# put them in a list and we will calculate the average
average_fscore = np.mean(f_score_list)
f_scoreStd = np.std(f_score_list)

average_fowlscore = np.mean(fowlesMallows_list)
fowlscore_std = np.std(fowlesMallows_list)

def countsPercentage(aList):
	"""
	Makes nice little counts of 1.0s and the percentage of their existence in the population
	"""
	count = Counter(aList)[1.0]
	percentage = Counter(aList)[1.0] / float(len(aList)) * 100.0

	return count, percentage


count1, percent1 = countsPercentage(f_score_list)
count2, percent2 = countsPercentage(fowlesMallows_list)

print count1, percent1, count2, percent2