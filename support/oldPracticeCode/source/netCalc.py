#!/usr/bin/python
"""
Author: Paul Scherer
Date: 19 Oct 2015

This module contains general mathematical functions for use with ppi networks
and their clustering algorithms
"""

import csv
import random
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from operator import mul

def calculateClassLikelihood(i, k, ppi_graph, pClasses, weight_matrix):
	"""
	For a protein i we define the likelihood of the cluster indicator z_i
	being set to cluster k given the current value of the weights matrix
	w as

	:param i: the name of the protein, accesses pClasses
	:param k: classId to measure our likelihood against
	:param ppi_graph: the networkx graph depicting the protein interactions
	:param pClasses: a dictionary with each protein and its assigned class
	:param weight_matrix: the symmetric weights matrix for the classes.
	"""
	# Neighbour weights
	# find the product of the weights of the neighbours of i
	neighbour_weights = []
	class_prot_a = k
	for neigh_a in ppi_graph.neighbors(i):
		class_neigh_a = pClasses[neigh_a]
		neighbour_weights.append(weight_matrix[class_prot_a, class_neigh_a])

	neighWeights_product = reduce(mul, neighbour_weights)
	neighbour_weights = []

	# Non neighbour weights
	non_neighbour_weights = []
	# remove the neighbours from the list of all nodes
	non_neighbours = [x for x in ppi_graph.nodes() if x not in ppi_graph.neigh(i)]
	non_neighbours.remove(i) # and the protein itself as well.
	for neigh_a in non_neighbours:
		class_non_neigh_a = pClasses[neigh_a]
		non_neighbour_weights.append(1 - weight_matrix[class_prot_a, class_non_neigh_a])

	non_neighWeights_product = reduce(mul, neighbour_weights)
	non_neighbour_weights = []

	# then calculate the likelihood. 
	likelihood_ik = neighWeights_product * non_neighbour_weights
	return likelihood_ik


def calculate_prob_weight(k,l,weight_matrix,ppi_graph,pClasses):
	"""
	Returns the likelihood of an element inside the weight matrix given
	the network and node properties.

	:param k: row
	:param l: column
	:param pClasses: a dictionary with each protein and its assigned class
					 ie the cluster indicator list
	:param ppi_graph: the networkx graph depicting the protein interactions
	:param weight_matrix: the symmetric weights matrix for the classes.
	"""
	
	#count the number of edges between nodes in clusters k to l
	num_KToL = 0
	for prot_a in ppi_graph.nodes():
		if pClasses[prot_a] == k:
			for neigh_a in ppi_graph.neighbors(prot_a):
				if pClasses[neigh_a] == l:
					num_KToL += 1
				else:
					continue
		else: 
			continue
	# count number of k and l in cluster indicators list
	num_zK = (pClasses.values()).count(k)
	num_zL = (pClasses.values()).count(l)
	num_not_KToL = num_zK*num_zL - num_KToL

	weight = weight_matrix[k,l]
	likelihood_weight = (weight ** num_KToL) * ((1-weight) ** num_not_KToL)

	return likelihood_weight 

def number_edgesKToL(k,l,pClasses,ppi_graph):
	num_KToL = 0 # we count edges from k class nodes to l class nodes
	for prot_a in ppi_graph.nodes():
		if pClasses[prot_a] == k:
			for neigh_a in ppi_graph.neighbors(prot_a):
				if pClasses[neigh_a] == l:
					num_KToL += 1
				else:
					continue
		else: 
			continue
	return num_KToL
