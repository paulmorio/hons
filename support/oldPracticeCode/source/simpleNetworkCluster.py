#!/usr/bin/python
"""
Author: Paul Scherer
Date: 19 Oct 2015

This module implements a simplified network clustering technique designed by 
Dr. Tom Thorne of The University of Edinburgh.
"""

import csv
import random
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from operator import mul
from netCalc import calculateClassLikelihood, calculate_prob_weight, number_edgesKToL

##################################################################
############              File Reading                ############
##################################################################

data_file = open('scOrgSmall.tab2.txt','r')
reader = csv.reader(data_file, delimiter = "\t")
table = list(reader)

##################################################################
############        Data Structure Creation           ############
##################################################################
# A {protein, [interactingProtein]} dictionary
# create dictionary of proteins and the proteins they were shown
# to have interactions with.
ppi_dict = {}
for row in table[1:]:
	prot_a = row[1]
	prot_b = row[2]
	if prot_a in ppi_dict:
		if prot_b in ppi_dict[prot_a]:
			pass
		else:
			ppi_dict[prot_a].append(prot_b)
	else:
		ppi_dict[prot_a] = [prot_b]

# Create a graph through adding the edges.
ppi_graph = nx.Graph()
protein_a_list = ppi_dict.keys()
for protein_a in protein_a_list:
	for protein_b in ppi_dict[protein_a]:
		ppi_graph.add_edge(protein_a, protein_b)


##################################################################
############         Clustering Algorithm             ############
##################################################################

# for a network A of n proteins we fix the number of clusters to k
num_clusters = 8

# A dictionary linking protein with the cluster it is assigned to
# for now a random assignment.
pClasses = {}
for row in table[1:]:
	prot_a = row[1]
	pClasses[prot_a] = random.choice(range(num_clusters))

# A k by k symmetric matrix of weights (the probability of a protein in
# in one cluster interacting with a protein in another)
weight_matrix = np.zeros((num_clusters,num_clusters))

# Setting the weights of the weight_matrix via sampling from a beta distribution
alpha = 2 
beta = 8
for k in range(num_clusters):
	for l in range(num_clusters):
		starterValue = np.random.beta(alpha, beta)
		weight_matrix[k,l] = starterValue
		weight_matrix[l,k] = starterValue

# for some set number of iterations
for iteration in xrange(10000):
	for prot_a in range(ppi_graph.nodes()):
		r = []
		for classId in range(num_clusters):
			likelihood_ik = calculateClassLikelihood(prot_a, classId, ppi_graph, pClasses, weight_matrix) 
			r.append(likelihood_ik)
		# normalise r
		norm_r = [float(j)/sum(r) for j in r]
		# sample zi as a random integer in range 1 to num_clusters with weights r
		# TODO (but how?)

		# calculate mk, the numnber of zi = k for k in 1..K
		list_mk = []
		for x in range(num_clusters):
			list_mk.append(pClasses.values().count(pClasses[x]))

		# calculate nk,l the number of interactions (i,j) where zi = k, and zj = l
		# for k,l in 1..num_clusters
		numkl = {}
		for k in range(num_clusters):
			for l in range(num_clusters):
				numkl[(k, l)] = number_edgesKToL(k,l,pClasses, ppi_graph) 

		for k in range(num_clusters):
			for l in range(num_clusters):
				newValue = np.random.beta(alpha + numkl[(k,l)], beta + (list_mk[k]*list_mk[l]-numkl[(k,l)]))
				weight_matrix[k,l] = newValue
				weight_matrix[l,k] = newValue







