#!/usr/bin/python
"""
Author: Paul Scherer
Date: 19 Oct 2015

This module reads in a tab delimited txt file with the first two columns and
makes a dictionary with first column and second column
"""

import csv
import networkx as nx
import matplotlib.pyplot as plt

data_file = open('scOrgSmall.tab2.txt','r')
reader = csv.reader(data_file, delimiter = "\t")
table = list(reader)

##################################################################
############        Data Structure Creation           ############
##################################################################
# A {protein, [interactingProtein]} dictionary
# create dictionary of proteins and the proteins they were shown
# to have interactions with.
ppi_dict = {}
for row in table[1:]:
	prot_a = row[1]
	prot_b = row[2]
	if prot_a in ppi_dict:
		if prot_b in ppi_dict[prot_a]:
			pass
		else:
			ppi_dict[prot_a].append(prot_b)
	else:
		ppi_dict[prot_a] = [prot_b]

# Create a graph through adding the edges.
ppi_graph = nx.Graph()
protein_a_list = ppi_dict.keys()
for protein_a in protein_a_list:
	for protein_b in ppi_dict[protein_a]:
		ppi_graph.add_edge(protein_a, protein_b)

# Draw the graph onto canvas
pos = nx.spring_layout(ppi_graph,k=0.9,iterations=20, scale=5)
nx.draw_networkx_nodes(ppi_graph, pos=pos, nodelist = ppi_graph.nodes())
nx.draw_networkx_edges(ppi_graph, pos=pos, edgelist = ppi_graph.edges())
nx.draw_networkx_labels(ppi_graph, pos=pos)

# Display the canvas
plt.axis('off')
plt.show()


##################################################################
############               Graph Analysis             ############
##################################################################
# find the nodes
nodes = ppi_graph.nodes()
# find the number of nodes
num_nodes = ppi_graph.number_of_nodes()
# finding the neighbours of a as a list
nei_a = ppi_graph.neighbors("851136")
# look at connected items
whatsConn = nx.connected_components(ppi_graph)
# look at degree of each node
nodeDegs_dict = nx.degree(ppi_graph)
# compute the clustering coefficient for each node
nx.clustering(ppi_graph)