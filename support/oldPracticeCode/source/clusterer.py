"""
author: Paul Scherer

This module controls a run of the clustering algorithm and thus provides a general 
template for the running of any other variant of clustering on applicable data. Which
in this case is are protein interaction networks.

A clustering is performed following a series of steps starting with when a data 
source is defined along with an appropiate data extractor created. The extractor will
work process the data into a datatype (a dictionary, or equivalent networkx graph) that
the numerous clustering algorithms can work with. The Clustering algorithm then assigns
a clustering to the data, and creates a new dictionary (or networkx graph for visualisation)

This new clustered data can now take two pathways. In one the 
"""