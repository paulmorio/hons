"""
Author: Paul Scherer
Date: 19 Oct 2015

Test Module
"""

def split_list(data, n):
    from itertools import combinations, chain
    for splits in combinations(range(1, len(data)), n-1):
        result = []
        prev = None
        for split in chain(splits, [None]):
            result.append(data[prev:split])
            prev = split
        yield result

def possibleTrees(n):
	sequence = range(1,n+1)
	if n < 3:
		return sequence
	if n == 3:
		return list(split_list(sequence,2)) + list(split_list(sequence,1))
	if n > 3:
		return list(split_list(sequence, 2)) + list(split_list(sequence, 3))

a = possibleTrees(6)
print a
print len(a)