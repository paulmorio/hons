#!/usr/bin/env python
# a bar plot with errorbars
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

N = 8
avgUnweighted = (0.077311055376, 0.330454590932, 0.320692628135, 0.413449053829, 0.356653562131, 0.335931868369, 0.260686170904, 0.339968797755)
unwStd = (0.019441943675, 0.230495869259, 0.116560449849, 0.259423443432, 0.288707518483, 0.172920289695, 0.2518655429, 0.173621480151)

avgICND = (0.0780981434522, 0.379879765899, 0.582488358395, 0.323197530834, 0.452925201724, 0.466990676052, 0.347665406855, 0.365490241901)
icndStd = (0.0210762774642 ,0.238768898776, 0.331730970561, 0.186880805585, 0.367595252506, 0.324910269546, 0.269984034747, 0.193432204769)

avgICNP = (0.0791308514681 ,0.372342765243, 0.580778222851,0.323728388624, 0.458961392488, 0.452160915688, 0.342982302979 , 0.365001027181)
icnpStd = (0.0119569593904 ,0.225234536356, 0.326803967096, 0.20417977225, 0.364773450477, 0.322560010687, 0.271049743782, 0.195243935481)

ind = np.arange(N)  # the x locations for the groups
width = 0.25       # the width of the bars

fig, ax = plt.subplots()
fig.set_figheight(10)
fig.set_figwidth(15)
rects1 = ax.bar(ind, avgUnweighted, width, color='r', yerr=unwStd)
rects2 = ax.bar(ind + width, avgICND, width, color='g', yerr=icndStd)
rects3 = ax.bar(ind + width + width, avgICND, width, color='b', yerr=icnpStd)

# add some text for labels, title and axes ticks
ax.set_ylabel('Average Fowlkes-Mallows Index Scores')
ax.set_title('Average Fowlkes-Mallows Index Scores per Method')
ax.set_xticks(ind + width)
ax.set_xticklabels(('Fixed K MCMC (K=8)','Variable K MCMC' ,'K-Clique Percolation (K=3)', 'MCODE', 'DPClus', 'IPCA', 'Graph Entropy', 'CoAch'), rotation=70)

ax.legend((rects1[0], rects2[0], rects3[0]), ('Unweighted', 'Weighted ICND', 'Weighted ICNP'))


def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)

plt.show()

# 0.0773110553766 0.019441943675
# 0.330454590932 0.230495869259
# 0.320692628135 0.116560449849
# 0.413449053829 0.259423443432
# 0.356653562131 0.288707518483
# 0.335931868369 0.172920289695
# 0.260686170904 0.2518655429
# 0.339968797755 0.173621480151

# 0.0780981434522 0.0210762774642
# 0.379879765899 0.238768898776
# 0.582488358395 0.331730970561
# 0.323197530834 0.186880805585
# 0.452925201724 0.367595252506
# 0.466990676052 0.324910269546
# 0.347665406855 0.269984034747
# 0.365490241901 0.193432204769

# 0.0791308514681 0.0119569593904
# 0.372342765243 0.225234536356
# 0.580778222851 0.326803967096
# 0.323728388624 0.20417977225
# 0.458961392488 0.364773450477
# 0.452160915688 0.322560010687
# 0.342982302979 0.271049743782
# 0.365001027181 0.195243935481


