#!/usr/bin/env python
# a bar plot with errorbars
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

N = 8
avgUnweighted = (0.0573545717064, 0.254959192939, 0.292139926789, 0.373342375818, 0.34228583866, 0.277352518927, 0.23470869678, 0.280228077267)
unwStd = (0.0246916808042, 0.123049582395, 0.114972521292, 0.271925549565, 0.286246195829, 0.187409914172, 0.250196849314, 0.185332874578)

avgICND = (0.0605029476491, 0.284959192939, 0.563131941062, 0.280201962033, 0.437948051723, 0.44826510901 , 0.319498665145, 0.325842470464)
icndStd = (0.0285698602408, 0.113049582395, 0.333434431182, 0.193993740453, 0.368967228267, 0.323783985813, 0.265036521229, 0.195198858897)

avgICNP = (0.0538235411975, 0.294959192939, 0.560293120284, 0.281998002976 , 0.443009807078 , 0.432916075569 , 0.315328709409, 0.325770122072)
icnpStd = (0.0143374440677, 0.132049582395, 0.330719998608, 0.206988017419, 0.365319501019, 0.322003302659, 0.265758224468, 0.198092259676)

ind = np.arange(N)  # the x locations for the groups
width = 0.25       # the width of the bars

fig, ax = plt.subplots()
fig.set_figheight(10)
fig.set_figwidth(15)
rects1 = ax.bar(ind, avgUnweighted, width, color='r', yerr=unwStd)
rects2 = ax.bar(ind + width, avgICND, width, color='g', yerr=icndStd)
rects3 = ax.bar(ind + width + width, avgICND, width, color='b', yerr=icnpStd)

# add some text for labels, title and axes ticks
ax.set_ylabel('Average f-Scores')
ax.set_title('Average f-Scores per Method')
ax.set_xticks(ind + width)
ax.set_xticklabels(('Fixed K MCMC (K=8)','Variable K MCMC' ,'K-Clique Percolation (K=3)', 'MCODE', 'DPClus', 'IPCA', 'Graph Entropy', 'CoAch'), rotation=70)

ax.legend((rects1[0], rects2[0], rects3[0]), ('Unweighted', 'Weighted ICND', 'Weighted ICNP'))


def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)

plt.show()



# 0.0573545717064 0.0246916808042
# 0.254959192939 0.123049582395
# 0.292139926789 0.114972521292
# 0.373342375818 0.271925549565
# 0.34228583866 0.286246195829
# 0.277352518927 0.187409914172
# 0.23470869678 0.250196849314
# 0.280228077267 0.185332874578

# 0.0605029476491 0.0285698602408
# 0.284959192939 0.113049582395
# 0.563131941062 0.333434431182
# 0.280201962033 0.193993740453
# 0.437948051723 0.368967228267
# 0.44826510901 0.323783985813
# 0.319498665145 0.265036521229
# 0.325842470464 0.195198858897

# 0.0538235411975 0.0143374440677
# 0.294959192939 0.132049582395
# 0.560293120284 0.330719998608
# 0.281998002976 0.206988017419
# 0.443009807078 0.365319501019
# 0.432916075569 0.322003302659
# 0.315328709409 0.265758224468
# 0.325770122072 0.198092259676
