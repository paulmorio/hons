# This module helps run all the clustering algorithms at once and store 
# the clusterings into the relevant folder if run from the project directory

#############################################
##########     Fixed K MCMC  ################
#############################################
# ARGUMENTS ARE data, num Clusters, alpha, beta, steps
# Fixed K MCMC Unweighted #
python clusterAlgorithms/fixed_k_mcmc.py data/network_ICND_1_wo_weights_wo_zero_weights.txt 8 2 8 10000 > clusterValidation/fixed_k_mcmc_10000

# Fixed K MCMC Weighted #
python clusterAlgorithms/fixed_k_mcmc_weighted.py data/network_ICND_1_wo_zero_weights.txt 8 2 8 10000 > clusterValidation/fixed_k_mcmc_weighted_10000_ICND
python clusterAlgorithms/fixed_k_mcmc_weighted.py data/network_ICNP_1_wo_zero_weights.txt 8 2 8 10000 > clusterValidation/fixed_k_mcmc_weighted_10000_ICNP


###############################################
##########     Variable K MCMC  ###############
###############################################
# ARGUMENTS ARE data, initial num Clusters, alpha, beta, steps
# Variable K MCMCUnweighted #
python clusterAlgorithms/variable_k_mcmc.py data/network_ICND_1_wo_weights_wo_zero_weights.txt 4 2 8 10000 > clusterValidation/variable_k_mcmc_10000
# Variable K MCMCWeighted #
python clusterAlgorithms/variable_k_mcmc_weighted.py data/network_ICND_1_wo_zero_weights.txt 4 2 8 10000 > clusterValidation/variable_k_mcmc_weighted_10000_ICND
python clusterAlgorithms/variable_k_mcmc_weighted.py data/network_ICNP_1_wo_zero_weights.txt 4 2 8 10000 > clusterValidation/variable_k_mcmc_weighted_10000_ICNP

# #################################################
# ##########    K Clique Percolation   ############
# #################################################
# # Clique Percolation Unweighted #
# # General can set k in the command 

# # K = 3 #
# # Clique Percolation unweighted #
# python clusterAlgorithms/clique_percolation.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > clusterValidation/cliq_uw_K3

# # Clique Percolation weighted #
# python clusterAlgorithms/clique_percolation_weighted_k_3.py data/network_ICND_1_wo_zero_weights.txt > clusterValidation/cliq_w_K3_ICND
# python clusterAlgorithms/clique_percolation_weighted_k_3.py data/network_ICNP_1_wo_zero_weights.txt > clusterValidation/cliq_w_K3_ICNP

# # K = 4 #
# # Clique Percolation unweighted #
# # python clusterAlgorithms/clique_percolation.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > clusterValidation/cliq_uw_K4

# # Clique Percolation weighted #
# #python clusterAlgorithms/clique_percolation_weighted_K_4.py data/network_ICND_1_wo_zero_weights.txt > clusterValidation/cliq_w_K4_ICND
# #python clusterAlgorithms/clique_percolation_weighted_K_4.py data/network_ICNP_1_wo_zero_weights.txt > clusterValidation/cliq_w_K4_ICNP

# #####################################
# ##########     MCODE   ##############
# #####################################
# # MCODE unweighted #
# python clusterAlgorithms/mcode.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > clusterValidation/mcode_uw

# # MCODE weighted #
# python clusterAlgorithms/mcode_weighted.py data/network_ICND_1_wo_zero_weights.txt > clusterValidation/mcode_w_ICND
# python clusterAlgorithms/mcode_weighted.py data/network_ICNP_1_wo_zero_weights.txt > clusterValidation/mcode_w_ICNP

# #####################################
# ##########     DPCLUS   #############
# #####################################
# # DPCLUS unweighted #
# python clusterAlgorithms/dpclus.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > clusterValidation/dpclus_uw

# # DPCLUS weighted #
# python clusterAlgorithms/dpclus_weighted.py data/network_ICND_1_wo_zero_weights.txt > clusterValidation/dpclus_w_ICND
# python clusterAlgorithms/dpclus_weighted.py data/network_ICNP_1_wo_zero_weights.txt > clusterValidation/dpclus_w_ICNP

# #####################################
# ##########     IPCA   ##############
# #####################################
# # IPCA unweighted #
# python clusterAlgorithms/ipca.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > clusterValidation/ipca_uw

# # IPCA weighted # 
# python clusterAlgorithms/ipca_weighted.py data/network_ICND_1_wo_zero_weights.txt > clusterValidation/ipca_w_ICND
# python clusterAlgorithms/ipca_weighted.py data/network_ICNP_1_wo_zero_weights.txt > clusterValidation/ipca_w_ICNP

# #############################################
# ##########     Graph Entropy   ##############
# #############################################
# # Graph Entropy unweighted # 
# python clusterAlgorithms/graph_entropy.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > clusterValidation/graph_entropy_uw

# # Graph Entropy weighted #
# python clusterAlgorithms/graph_entropy_weighted.py data/network_ICND_1_wo_zero_weights.txt > clusterValidation/graph_entropy_w_ICND
# python clusterAlgorithms/graph_entropy_weighted.py data/network_ICNP_1_wo_zero_weights.txt > clusterValidation/graph_entropy_w_ICNP

# #####################################
# ##########     CoAch   ##############
# #####################################
# # CoAch unweighted #
# python clusterAlgorithms/coach.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > clusterValidation/coach_uw

# # CoAch weighted # 
# python clusterAlgorithms/coach_weighted.py data/network_ICND_1_wo_zero_weights.txt > clusterValidation/coach_w_ICND
# python clusterAlgorithms/coach_weighted.py data/network_ICNP_1_wo_zero_weights.txt > clusterValidation/coach_w_ICNP