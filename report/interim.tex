\documentclass[bsc,logo,twoside,singlespacing,parskip]{infthesis} % for BSc, BEng etc.
\usepackage[]{algorithmic, graphicx, float}
%http://imgur.com/gallery/rBPo4
%frontabs
%deptreport
\begin{document}

\title{Protein Interaction Network Clustering \\ Interim Report}
\author{Paul Scherer}
\course{Artificial Intelligence and Computer Science}
\project{4th Year Project Report}
\date{\today}

\abstract{
It is well known that interactions between proteins are an essential component in the execution of biological functions, and a core focus of study in systems biology. The description of these interactions would greatly contribute to the functional interpretation the proteins, the genes that encode them, as well as a greater understanding of cellular organizations, processes, and functions \cite{zhang2009}. High throughput experimental studies such as the two-hybrid screening to identify in protein-protein interactions (PPI) from the genome sequence of the Saccharomyces Cerevisiae \cite{uetz2000} have resulted in creation of datasets with complex undirected networks of interactions between proteins. It is possible to detect community structures within these networks through computational analysis. This paper will introduce several clustering techniques to PPI network data to predict functions of proteins or identify protein complexes, as well as implement some discussed in other works. The results of the clusterings created are then assessed using a variety of external and internal evaluation techniques that have been implemented for the purposes of this project.
}
\maketitle

%\section*{Acknowledgements}
%Acknowledgements go here. I will fill this out as I get closer to completing the report.

\tableofcontents
%\pagenumbering{arabic}

\chapter{Interim Report Content}

The principal goal of the project is to develop clustering methods applicable to protein interaction networks.

Completion Criteria: Implementation of a protein interaction network clustering method and comparison to other existing methods.

This section will discuss the work that has been completed for this project thus far, giving reasons to some of the decisions made and explaining drawbacks. The following chapter will give a brief skeleton structure of the final report.

\subsection{Implementation}
This section discusses the details of implementation for the experiments to be made later in the project. The implementation is nearly complete though it has been very challenging to translate many of the clustering techniques into python and subsequently visualising them. The other large problem from an architectural and goal perspective is the evaluation of the clusterings created. Each of the problems were tackled on their own as snippets due to heavy time constraints in the last semester to work and complete something whenever there was time. Thus this semester is more focused on the consolidation and integration of the individually created snippets in clustering, visualisation, and evaluation.

Much of the code has been decoupled as much as possible situated around a framework that can take in different modules for the different tasks of data extraction and cleaning, exploratory data analysis (the clustering algorithms), data visualisation, and validation measures. There is a slight information overlap from the dictionaries used to represent the networks and the Graph datatype used for visualising this same graph in NetworkX but this was done on purpose for the purpose of developing concurrently and because data visualisation is envisaged to be the step right after the clustering has been completed and data from the clusterings will be copied and represented into the Graph datatype at this stage. An abstract pipeline of the process has been provided in Figure 1.
 
\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{proteinpipeline.png}
  \caption{Abstract pipeline of the processes involved in the system implemented.}
  \label{fig:lol}
\end{figure}

\subsubsection{Clustering Algorithms}
3 Clustering algorithms applicable to the PPI networks have been decided upon and implemented in this project thus far. Two have been made by Dr. Thorne, and a K-Clique based clustering algorithm created on the description of a paper by G.Palla et al \cite{Palla2005}. All of the clustering techniques thus far have been graph based clustering methods in order to deal with the binary nature of the protein interaction information in the datasets.

\subsubsection{Data Visualisation}
In order to visualise PPI network graphs, the Python NetworkX package was used in conjunction with MatPlotLib to display the created graphs onto the screen, and has become central to the data representation of the networks themselves. Each protein inside of the network has been represented as a node and each binary connection between proteins, a recorded interaction between them as an edge. This contrasts with the original design of having a dictionary of lists where an entry in the dictionary was made for each protein followed by a list of the proteins this protein had in the network. 

Visualising the clusters themselves posed a significantly difficult task due to information between the interaction of proteins being binary, the proteins simply interact or don't. As a result there is no logical placing of the nodes themselves that would make the graph look logical. Hence randomly placing the nodes in a spring order produced the graph in Figure 2. 

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{initial_graph.png}
  \caption{Placement of 6000 proteins and their interactions in Spring Layout produced in NetworkX}
  \label{fig:1-1hist}
\end{figure}

Getting to this stage was surprisingly difficult as it involved using quite a few libraries and working out their dependencies which was helped least by a confusing error caused by using MatPlotLib the current (at October 2015) against the newest edition of numpy (at October 2015). In particular it would not allow NetworkX to render more than 50 nodes of the network. This problem was raised by someone else using numpy and matplotlib in issue 5209. After figuring this out I had downgraded my version of Numpy which allowed the rendering of the nodes to continue, and the issue has now been fixed, hence the system will be upgraded to the newest versions of the libraries at the earliest convenience.

In order to visualise the network and clusters better several suggestions were made. For the implementation of clustering with a fixed number of clusters it was suggested that the cluster central locations be set, and nodes (proteins) belonging to any particular cluster would be placed randomly close to this location. This would highlight the different clusters, their relative size and the inter cluster interactions. However this would make intra cluster connections hard to see, which are important for looking at protein complexes. Another suggestion was to put densely connected subgraphs together highly connected nodes towards the centre of the network which could be applied to any of the clustering algorithms fixed or dynamic, but this has proven difficult to implement due to challenges in the placement. Colours seem to be a recurring problem in the rendering of the clustering task.

\subsubsection{Evaluation}
Evaluation of clustering algorithms, or rather cluster validation, is an inherently difficult task as the desired characteristics depend on the particular problem under consideration \cite{Manning2008}. Any clustering could be considered "good" or "bad" without a basis of what one is looking for; without a gold standard one can compare to, looking at the clusterings created are more of exploratory interest and for hypothesis generation. This of course makes sense as the cluster analysis is inherently a set of techniques used in exploratory data analysis. Evaluation would have to be experimental and this has been done before using classic validation measures widely used in literature \cite{Pizzuti2014}. 

 A search for a gold standard in clustering of protein complexes and/or for protein function was made but with no success; and I would like to get help on finding these if any are available and I have simply missed them. Otherwise, it could be possible to cluster the proteins and see if they reflect any of the characteristics that are present in the dataset already. Nonetheless for the event that something can be found several metrics for internal and external validation were implemented following standard cluster analysis validation techniques for graph based clusterings based on the paper by C. Lin et al \cite{clusterreview}.

\textbf{Internal Validation}: Internal validation technique evaluate the clustering based on the data that was clustered itself, usually assigning best scores to algorithms that produce clusters with high similarity within a cluster and low similarity between clusters. Here the following graph based validation measures \cite{clusterreview} were implemented.
\begin{itemize}
\item Validation Based on Definition of Clustering (may not work due to necessity of a "centroid" which may be decided arbitrarily, making the measure wholly redundant)
\item Reliability of Clusters \cite{RevModPhys}
\item Leave one out method
\end{itemize}

\textbf{External Validation}: For external validation to work one would necessitate the presence of a gold standard or similar benchmark to compare against. For this purpose these classic measures have been implemented.
\begin{itemize}
\item Rand Measure
\item F-Measure
\item Jaccard Index
\item Fowles-Mallows index
\end{itemize}
often confusion matrices are used for these tasks but it was deemed difficult to use as one of the clustering algorithms utilises a non fixed number of clusters.


\subsection{Other things that have been completed or are in progress}
Work has been made to write up a background and introduction to this subject with focus on explaining the biological terminology, and clustering techniques so that the paper would be accessible (firsly to me who had trouble tackling both of these topics) to provide enough context and information to allow an undergraduate informatics student to understand it. The bulk of the background literature reading will be stated here; with the rest being allocated to a "related work" section after the results so that comparative analysis can be made between the contributions made in the paper and the work of others.  

\section{Plans for the Upcoming Deadlines}
During the second semester of my studies I will be taking on only 2 courses, as a result of having taken 60 credits last semester, and will thus finally be able to focus on working and completing the project. Generally I intend to spend much of the next 1.5 months working exclusively on completing the implementations and running experiments to get results. Finally using the remaining time to write the report early to get some feedback (hopefully I will be able to do this as I go along as well). This has not been achieved yet due to lack of work from and developing the clustering algorithms, data visualisations, and evaluation codes independently, working on one when I ran into a dead-end for another. 

\subsection{Time Table For Work to be Completed}
\begin{itemize}
	\item Feb 07: Work on integrating the clustering algorithm with the evaluation sets to return results, that can be displayed in the report.
	\item Feb 12: Integrate the clustering algorithm results with the data visualisation code, and integrate results into the report.
	\item Feb 19: Continue reading on both protein protein interaction networks in order to write about the different clustering algorithms they have implemented and their motivations. (MCODE, DBSCAN, etc.) In particular look for clustering algorithms similar ones used in paper. This is for the discussion and conclusion section. 
	\item Feb 29: Have a rough draft of the report. In particular this will feature the introduction, background, implementation details, methodology, results. However it will not have a lot of analysis in the evaluation or a discussion section to discuss the work of other people in relation to the clustering algorithms contributed.
\end{itemize}

\chapter{Intended Skeleton Structure of Report}
In this chapter I intend to outline the sections I envision to have in the final report. Each of the sections will contain basic description of what will be included therein. This is of course very coarse outline, and subject to a lot of change; but nonetheless reflects how I think the content could be spread out in a logical manner. As a technical note, each section shown below will be a chapter, subsection a section, and so on.

\section{Introduction}
This section will introduce a problem that a clustering algorithm can solve, and leads into why creating clustering algorithms applicable to protein interaction networks is interesting and worthwhile. It will be written with the intention to be accessible to people both experienced and new to clustering algorithms as well as the protein interaction networks they are applied to.

\subsection{Background}
This subsection expands on the specific problem given in the introduction. It serves to introduce the reader to protein interaction networks from the viewpoint of an informatics student, basic clustering ideas and concepts, and the intrinsic challenges posed by clustering these networks. One such problem is the lack of a notion of distance between two proteins that interact in most databases. Another problem is the distinction between the types of protein interactions that exist and what to do with an incomplete dataset, that doesn't have conclusive information on the nature of the interactions.

This background will lead up to the description of the work completed in this project and its assumptions and constraints. A basic first draft of this has been written above to exemplify writing style. 

\subsection{Contributions}
Brief discussion of applying solutions created in this project to the problem given in the introduction. Then summarizing in bullet point format what has been built, and what it intends to solve. 
\begin{itemize}
\item Clusterings: Different clustering algorithms have been implemented with the intention of providing clusterings biological data in the form of protein interaction networks.
\item Data: For this report the clustering algorithms were applied to the protein interaction data created via high throughput yeast two-hybrid screens. The data was taken from BioGrid. Algorithms also work on other data taken from the BioGrid and other protein interaction network sources provided that a data extraction/cleaning module has been written for the schema utilized by the database.
\item Evaluation: Evaluation was performed in a variety of ways with even more still pending and in suggestion due to the nature of the clustering (more to this above).
\end{itemize}

\section{Implementation}
\subsection{Tech Stack}
Description of the set of tools used in this project
\subsection{Data Extraction}
Description of methods used to extract the information, or rather what, and in what format was extracted from the PPI files.
\subsection{Clustering Algorithms}
In this subsection I will describe each of the algorithms implemented from a mathematical standpoint. Writing the implementation in pseudocode for the purpose of explaining the processes underlying and motivations behind particular steps.
\subsubsection{Clustering Algorithm 1}
Implementation Description and Motivations for Clustering Algorithm 1 (Fixed Clusters)
\subsubsection{Clustering Algorithm 2}
Implementation Description and Motivations for Clustering Algorithm 2 (Non Fixed Clusters)
\subsubsection{Clustering Algorithm 3}
Implementation Description and Motivations for Clustering Algorithm 3 (Implemented K Clique Cluster Algorithm)
\subsubsection{Problems}
Discuss problems encountered during the implementation process including the numpy1.50 elementwise comparison bug or having to implement the clustering algorithms of other work.

\subsection{Data Visualization}
Explanation on how the Data Visualisation was implemented, as well as the challenges that came out of this.

\section{Methodology}
The Methodology section will lay out the different experiments and benchmarks set up for the comparison of the different clustering algorithms and their "performance" according to the validation calculations set up. ie here the different benchmarks will be introduced, with motivations as to why they were deemed appropiate for comparing the different clustering algorithms.

\section{Results}
This will display the results and discussion of the results for each algorithm or altogether on the same dataset. If possible will include the clustering algorithms described by other related work that have been implemented. Moreover analysis and some hypotheses to explain the results will be made. 

\section{Related Work}
This is not so much a literature review but a discussion of inspirational work that has been completed previously by researchers who have also written about clustering techniques applicable to Protein Interaction Networks. This will look at the clustering algorithms they have created, their assumptions, differences to the algorithms here, etc.

\section{Conclusions}
In the conclusion I will summarise the contributions made, take into account some wider considerations, and make suggestions for future work.

% use the following and \cite{} as above if you use BibTeX
% otherwise generate bibtem entries
\bibliographystyle{plain}
\bibliography{mybibfile}

\end{document}
