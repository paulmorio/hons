import matplotlib.pyplot as plt
import numpy as np
import sys
import os

pubNums = []
years = []

for line in sys.stdin:
	line = line.strip()
	line = line.strip("\n")
	pubs, year, dc = line.split()
	print pubs, year

	pubNums.append(pubs)
	years.append(year)
	
pubNums = pubNums[::-1]
years = years[::-1]

plt.plot(years, pubNums)
plt.show()