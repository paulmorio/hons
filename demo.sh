# Demo for Presentation
# By Paul Scherer

#Look at the Data that comes in from the databases providing the PPI data
#Look at the cleaned versions after CSV run
#Discuss running the clustering algorithm

########################################################################################################################
#############################                      Clustering                        ###################################
########################################################################################################################

#################################################
##########    K Clique Percolation   ############
#################################################
# Clique Percolation Unweighted #
# General can set k in the command 

# K = 3 #
# Clique Percolation unweighted #
python clusterAlgorithms/clique_percolation.py data/network_ICND_1_wo_weights_wo_zero_weights.txt 3 demo/cliq_uw_K3

# Clique Percolation weighted #
python clusterAlgorithms/clique_percolation_weighted_k_3.py data/network_ICND_1_wo_zero_weights.txt > demo/cliq_w_K3_ICND
python clusterAlgorithms/clique_percolation_weighted_k_3.py data/network_ICNP_1_wo_zero_weights.txt > demo/cliq_w_K3_ICNP

#####################################
##########     MCODE   ##############
#####################################
# MCODE unweighted #
python clusterAlgorithms/mcode.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > demo/mcode_uw

# MCODE weighted #
python clusterAlgorithms/mcode_weighted.py data/network_ICND_1_wo_zero_weights.txt > demo/mcode_w_ICND
python clusterAlgorithms/mcode_weighted.py data/network_ICNP_1_wo_zero_weights.txt > demo/mcode_w_ICNP

#####################################
##########     DPCLUS   #############
#####################################
# DPCLUS unweighted #
python clusterAlgorithms/dpclus.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > demo/dpclus_uw

# DPCLUS weighted #
python clusterAlgorithms/dpclus_weighted.py data/network_ICND_1_wo_zero_weights.txt > demo/dpclus_w_ICND
python clusterAlgorithms/dpclus_weighted.py data/network_ICNP_1_wo_zero_weights.txt > demo/dpclus_w_ICNP

#####################################
##########     IPCA   ##############
#####################################
# IPCA unweighted #
python clusterAlgorithms/ipca.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > demo/ipca_uw

# IPCA weighted # 
python clusterAlgorithms/ipca_weighted.py data/network_ICND_1_wo_zero_weights.txt > demo/ipca_w_ICND
python clusterAlgorithms/ipca_weighted.py data/network_ICNP_1_wo_zero_weights.txt > demo/ipca_w_ICNP

#############################################
##########     Graph Entropy   ##############
#############################################
# Graph Entropy unweighted # 
python clusterAlgorithms/graph_entropy.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > demo/graph_entropy_uw

# Graph Entropy weighted #
python clusterAlgorithms/graph_entropy_weighted.py data/network_ICND_1_wo_zero_weights.txt > demo/graph_entropy_w_ICND
python clusterAlgorithms/graph_entropy_weighted.py data/network_ICNP_1_wo_zero_weights.txt > demo/graph_entropy_w_ICNP

#####################################
##########     CoAch   ##############
#####################################
# CoAch unweighted #
python clusterAlgorithms/coach.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > demo/coach_uw

# CoAch weighted # 
python clusterAlgorithms/coach_weighted.py data/network_ICND_1_wo_zero_weights.txt > demo/coach_w_ICND
python clusterAlgorithms/coach_weighted.py data/network_ICNP_1_wo_zero_weights.txt > demo/coach_w_ICNP

########################################################################################################################
##########################                     INTERNAL Validation                        ##############################
########################################################################################################################

# K Clique Percolation
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/cliq_uw_K3  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/cliq_uw_K3
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/cliq_w_K3_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/cliq_w_K3_ICNP  

# MCODE
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/mcode_uw
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/mcode_w_ICNP  

# DPClus
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/dpclus_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/dpclus_uw
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/dpclus_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/dpclus_w_ICNP  

# IPCA
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/ipca_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/ipca_uw 
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/ipca_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/ipca_w_ICNP  


# Graph Entropy
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/graph_entropy_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/graph_entropy_uw 
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/graph_entropy_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/graph_entropy_w_ICNP  

# CoAch
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/coach_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/coach_uw
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/coach_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/coach_w_ICNP  




########################################################################################################################
#############################                     External Validation                        ###########################
########################################################################################################################

# # Fixed K MCMC Clustering
python externalValidation.py demo/fixed_k_mcmc_10 data/ground_truth.txt
python externalValidation.py demo/fixed_k_mcmc_weighted_10_ICND data/ground_truth.txt
python externalValidation.py demo/fixed_k_mcmc_weighted_10_ICNP data/ground_truth.txt 

# # Variable K MCMC Clustering
python externalValidation.py cleanClusters1 data/ground_truth.txt
python externalValidation.py cleanClusters2 data/ground_truth.txt
python externalValidation.py cleanClusters3 data/ground_truth.txt 

# K Clique Percolation
python externalValidation.py demo/cliq_uw_K3 data/ground_truth.txt 
python externalValidation.py demo/cliq_w_K3_ICND data/ground_truth.txt 
python externalValidation.py demo/cliq_w_K3_ICNP data/ground_truth.txt 

# MCODE
python externalValidation.py demo/mcode_uw data/ground_truth.txt 
python externalValidation.py demo/mcode_w_ICND data/ground_truth.txt 
python externalValidation.py demo/mcode_w_ICNP data/ground_truth.txt 

# DPClus
python externalValidation.py demo/dpclus_uw data/ground_truth.txt 
python externalValidation.py demo/dpclus_w_ICND data/ground_truth.txt 
python externalValidation.py demo/dpclus_w_ICNP data/ground_truth.txt 

# IPCA
python externalValidation.py demo/ipca_uw data/ground_truth.txt 
python externalValidation.py demo/ipca_w_ICND data/ground_truth.txt 
python externalValidation.py demo/ipca_w_ICNP data/ground_truth.txt 


# Graph Entropy
python externalValidation.py demo/graph_entropy_uw data/ground_truth.txt 
python externalValidation.py demo/graph_entropy_w_ICND data/ground_truth.txt 
python externalValidation.py demo/graph_entropy_w_ICNP data/ground_truth.txt 

# CoAch
python externalValidation.py demo/coach_uw data/ground_truth.txt 
python externalValidation.py demo/coach_w_ICND data/ground_truth.txt 
python externalValidation.py demo/coach_w_ICNP data/ground_truth.txt 


########################################################################################################################
#############################                     Visualisation                        #################################
########################################################################################################################

# K Clique Percolation
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt demo/cliq_w_K3_ICND yamlFiles/cliqICND.yaml 0.001 legendFolder/cliqLegend
python plot/modDraw.py yamlFiles/cliqICND.yaml demo/cliq_w_K3_ICND cliqICNDColor.pdf legendFolder/cliqLegend

# MCODE
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend
python plot/modDraw.py yamlFiles/mcodeICND.yaml demo/mcode_w_ICND mcodeICNDColor.pdf legendFolder/mcodeLegend

# DPClus
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt demo/dpclus_w_ICND yamlFiles/dpClusICND.yaml 0.001 legendFolder/dpclusLegend
python plot/modDraw.py yamlFiles/dpClusICND.yaml demo/dpclus_w_ICND dpClusICNDColor.pdf legendFolder/dpclusLegend

# IPCA
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt demo/ipca_w_ICND yamlFiles/ipcaICND.yaml 0.001 legendFolder/ipcaLegend
python plot/modDraw.py yamlFiles/ipcaICND.yaml demo/ipca_w_ICND ipcaICNDColor.pdf legendFolder/ipcaLegend

# Graph Entropy
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt demo/graph_entropy_w_ICND yamlFiles/graphEntropyICND.yaml 0.001 legendFolder/graphEntropyLegend
python plot/modDraw.py yamlFiles/graphEntropyICND.yaml demo/graph_entropy_w_ICND graphEntropyICNDColor.pdf legendFolder/graphEntropyLegend

# CoAch
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt demo/coach_w_ICND yamlFiles/coachICND.yaml 0.001 legendFolder/coachLegend
python plot/modDraw.py yamlFiles/coachICND.yaml demo/coach_w_ICND coachICNDColor.pdf legendFolder/coachLegend


#### Normal with only cluster edges ####
# Fixed K MCMC Clustering

# Variable K MCMC Clustering


# # K Clique Percolation
# python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt demo/cliq_w_K3_ICND yamlFiles/cliqICND.yaml 0.001 legendFolder/cliqLegend 241
# python plot/modDraw.py yamlFiles/cliqICND.yaml demo/cliq_w_K3_ICND cliqICNDcedge.pdf legendFolder/cliqLegend

# MCODE
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend 81
python plot/modDraw.py yamlFiles/mcodeICND.yaml demo/mcode_w_ICND mcodeICNDcedgeColor.pdf legendFolder/mcodeLegend

# DPClus
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt demo/dpclus_w_ICND yamlFiles/dpClusICND.yaml 0.001 legendFolder/dpclusLegend 713
python plot/modDraw.py yamlFiles/dpClusICND.yaml demo/dpclus_w_ICND dpClusICNDcedgeColor.pdf legendFolder/dpclusLegend

# IPCA
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt demo/ipca_w_ICND yamlFiles/ipcaICND.yaml 0.001 legendFolder/ipcaLegend 1420
python plot/modDraw.py yamlFiles/ipcaICND.yaml demo/ipca_w_ICND ipcaICNDcedgeColor.pdf legendFolder/ipcaLegend

# Graph Entropy
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt demo/graph_entropy_w_ICND yamlFiles/graphEntropyICND.yaml 0.001 legendFolder/graphEntropyLegend 644
python plot/modDraw.py yamlFiles/graphEntropyICND.yaml demo/graph_entropy_w_ICND graphEntropyICNDcedgeColor.pdf legendFolder/graphEntropyLegend

# CoAch
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt demo/coach_w_ICND yamlFiles/coachICND.yaml 0.001 legendFolder/coachLegend 1730
python plot/modDraw.py yamlFiles/coachICND.yaml demo/coach_w_ICND coachICNDcedgeColor.pdf legendFolder/coachLegend


#### Subgraphs of Clustered Nodes ####
# Fixed K MCMC Clustering

# Variable K MCMC Clustering

# K Clique Percolation
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt demo/cliq_w_K3_ICND yamlFiles/cliqICND.yaml 0.001 legendFolder/cliqLegend
python plot/modDraw.py yamlFiles/cliqICND.yaml demo/cliq_w_K3_ICND cliqICNDsubColor.pdf legendFolder/cliqLegend

# MCODE
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend
python plot/modDraw.py yamlFiles/mcodeICND.yaml demo/mcode_w_ICND mcodeICNDsubColor.pdf legendFolder/mcodeLegend

# DPClus
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt demo/dpclus_w_ICND yamlFiles/dpClusICND.yaml 0.001 legendFolder/dpclusLegend
python plot/modDraw.py yamlFiles/dpClusICND.yaml demo/dpclus_w_ICND dpClusICNDsubColor.pdf legendFolder/dpclusLegend

# IPCA
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt demo/ipca_w_ICND yamlFiles/ipcaICND.yaml 0.001 legendFolder/ipcaLegend
python plot/modDraw.py yamlFiles/ipcaICND.yaml demo/ipca_w_ICND ipcaICNDsubColor.pdf legendFolder/ipcaLegend

# Graph Entropy
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt demo/graph_entropy_w_ICND yamlFiles/graphEntropyICND.yaml 0.001 legendFolder/graphEntropyLegend
python plot/modDraw.py yamlFiles/graphEntropyICND.yaml demo/graph_entropy_w_ICND graphEntropyICNDsubColor.pdf legendFolder/graphEntropyLegend

# CoAch
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt demo/coach_w_ICND yamlFiles/coachICND.yaml 0.001 legendFolder/coachLegend
python plot/modDraw.py yamlFiles/coachICND.yaml demo/coach_w_ICND coachICNDsubColor.pdf legendFolder/coachLegend