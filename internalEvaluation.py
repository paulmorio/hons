# # Author: Paul Scherer (paulmorio)
# This module calculates cluster properties of clusters in the clusterings
# given a data file and the clustering made of it

# Currrently the following are calculated
#  - number of clusters found
#  - Average size of Clusters as well as the standard deviation of this value
#  - Size of the biggest cluster
#  - Average density as well as Average Weighted Density of the Clusters
#  - Number of Proteins Covered 

import sys
import numpy
import networkx as nx

arg = sys.argv
dataFile = arg[1]
clusteringFile = arg[2]

graph = nx.read_weighted_edgelist(dataFile)
weights = nx.get_edge_attributes(graph, 'weight')

# Open Clustering file and calculate clusterCount, avgSize, StdDev, maxSize, + 
f = open(clusteringFile, 'r')
proteinsList = []
proteinsCovered = 0
clusterCount = 0
clusterList = []
for line in f:
	line.strip('\n')
	cluster = line.split()
	
	for x in cluster:
		if (x not in proteinsList):
			proteinsCovered += 1
		proteinsList.append(x)

	clusterList.append(cluster)
	clusterCount += 1
f.close()

# get the size of the biggest cluster
maxSize = len(max(clusterList,key=len)) # HAHA dat kanker jevel runtime

# Calculate the average size of the clusters
clusterLens = [len(x) for x in clusterList]
avgSize = numpy.mean(clusterLens)
stdDev = numpy.std(clusterLens)

# RUNS INTO VERY INTERESTING NETWORKX BUGS NAMELY THAT SUBGRAPHS ARE BROKEN BEYOND REPAIR
# Calculate the average density and average weighted density of each graph.
# def weighted_density(subgraph):
# 	num_nodes = graph.number_of_nodes()
# 	maximal_connections = num_nodes * (num_nodes-1)
# 	subgraph_weights = nx.get_edge_attributes(subgraph,'weight')
# 	subgraph_weight_list = []

# 	for edge in subgraph_weights.keys():
# 		subgraph_weight_list.append(subgraph_weights[edge])

# 	weighted_edges = 2 * sum(subgraph_weight_list)
# 	weightedDensity = float(weighted_edges) / maximal_connections
# 	return weightedDensity 

def weighted_density(g):
	"""
	returns the weighted density of a graph g = (V,E)
	defined as 
		:math:
		den(g) = \frac{2*sum(weighted_edges)}{|V| * |V-1|}
	"""
	without_weights = float(nx.density(g) / g.number_of_edges())
	g_weights = nx.get_edge_attributes(g,'weight')
	weights_list=[]
	for edge in g_weights.keys():
		weights_list.append(g_weights[edge])

	edge_weights_sum = sum(weights_list)
	wden = without_weights * edge_weights_sum

	return wden

densityList = []
weightedDensityList = []
for x in clusterList:
	densityList.append(nx.density(graph.subgraph(x)))
	weightedDensityList.append(weighted_density(graph.subgraph(x)))

avgDen = numpy.mean(densityList)
avgWeightedDen = numpy.mean(weightedDensityList)

# Output at the end of it
print str(clusterCount) + "\t" + str(avgSize) + "\t" + str(stdDev) + "\t" + str(maxSize) + "\t" + str(avgDen) + "\t" + str(avgWeightedDen) + "\t" + str(proteinsCovered)

