# demo 2 Focus on MCODE for time reasons in presentation.
# Demo for Presentation
# By Paul Scherer

#Look at the Data that comes in from the databases providing the PPI data
#Look at the cleaned versions after CSV run
#Discuss running the clustering algorithm

########################################################################################################################
#############################                      Clustering                        ###################################
########################################################################################################################

#####################################
##########     MCODE   ##############
#####################################
# MCODE unweighted #
python clusterAlgorithms/mcode.py data/network_ICND_1_wo_weights_wo_zero_weights.txt > demo/mcode_uw

# MCODE weighted #
python clusterAlgorithms/mcode_weighted.py data/network_ICND_1_wo_zero_weights.txt > demo/mcode_w_ICND
python clusterAlgorithms/mcode_weighted.py data/network_ICNP_1_wo_zero_weights.txt > demo/mcode_w_ICNP

########################################################################################################################
##########################                     INTERNAL Validation                        ##############################
########################################################################################################################
# MCODE
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_uw  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/mcode_uw
python internalEvaluation.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_w_ICND  
python internalEvaluation.py data/network_ICNP_1_wo_zero_weights.txt demo/mcode_w_ICNP  

########################################################################################################################
#############################                     External Validation                        ###########################
########################################################################################################################

# MCODE
python externalValidation.py demo/mcode_uw data/ground_truth.txt 
python externalValidation.py demo/mcode_w_ICND data/ground_truth.txt 
python externalValidation.py demo/mcode_w_ICNP data/ground_truth.txt 

########################################################################################################################
#############################                     Visualisation                        #################################
########################################################################################################################
# MCODE
python plot/modBundle.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend
python plot/modDraw.py yamlFiles/mcodeICND.yaml demo/mcode_w_ICND mcodeICNDColor.pdf legendFolder/mcodeLegend

# MCODE
python plot/modBundle_cedge.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend 81
python plot/modDraw.py yamlFiles/mcodeICND.yaml demo/mcode_w_ICND mcodeICNDcedgeColor.pdf legendFolder/mcodeLegend

# MCODE
python plot/subgraph_edition.py data/network_ICND_1_wo_zero_weights.txt demo/mcode_w_ICND yamlFiles/mcodeICND.yaml 0.001 legendFolder/mcodeLegend
python plot/modDraw.py yamlFiles/mcodeICND.yaml demo/mcode_w_ICND mcodeICNDsubColor.pdf legendFolder/mcodeLegend