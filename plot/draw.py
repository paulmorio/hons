import math
import colorsys
import yaml
import cairo
import sys

arg=sys.argv
infile=arg[1]
infileC=arg[2]
outfile=arg[3]


#load
f=open(infile,'r')
net=yaml.load(f)
f.close()

cix=[]
f=open(infileC,'r')
for l in f:
    cix.append(float(l))
f.close()

degs={}

for a in net['nodes']:
	degs[a]=0
for [a,b,w] in net['edges']:
        if a in degs:
                degs[a] = degs[a]+1
        else:
                degs[a] = 1
        if b in degs:
                degs[b] = degs[b]+1
        else:
                degs[b] = 1

def netdraw(net,xs,ys,file):
	surface=cairo.PDFSurface(file,xs,ys)
	#clear
	ctx=cairo.Context(surface)
    	ctx.scale(xs,ys) #coordinates all 0.0->1.0
    	ctx.rectangle(0,0,1,1)
    	ctx.set_source_rgb(1.0,1.0,1.0)
    	ctx.fill()
	ctx.set_line_join(cairo.LINE_JOIN_ROUND)
    	#
       	#edges
    	for [a,b,w] in net['edges']:
		xa=2.0*(float(net['nodes'][a]['x'])-0.5)+0.5
		ya=2.0*(float(net['nodes'][a]['y'])-0.5)+0.5
		xb=2.0*(float(net['nodes'][b]['x'])-0.5)+0.5
		yb=2.0*(float(net['nodes'][b]['y'])-0.5)+0.5
		ctx.move_to(xa,ya)
                ctx.line_to(xb,yb)
        	ctx.set_source_rgba(0.4,0.4,0.4,w)
    		ctx.set_line_width(0.001)
                ctx.stroke()
        #nodes
	r = 0.001
    	for i in net['nodes']:
                x=2.0*(float(net['nodes'][i]['x'])-0.5)+0.5
                y=2.0*(float(net['nodes'][i]['y'])-0.5)+0.5
                d=degs[i]
                if d>10:
                        d = 10
		#outline
                # rgb=colorsys.hsv_to_rgb(0.7*(cix[i]-1)/max(cix),1.0,1.0)
                # rgbb=colorsys.hsv_to_rgb(0.7*(cix[i]-1)/max(cix),0.5,1.0)
                rgb=colorsys.hsv_to_rgb(0.7*(cix[i]-1)/10,1.0,1.0)
                rgbb=colorsys.hsv_to_rgb(0.7*(cix[i]-1)/10,0.5,1.0)
                rr = r + d*0.0003
		ctx.arc(float(x),float(y),rr,0,2*math.pi)
    		ctx.set_line_width(0.003)
		ctx.set_source_rgb(*rgb)
        	ctx.stroke()
		ctx.arc(float(x),float(y),rr,0,2*math.pi)
		ctx.set_source_rgb(*rgbb)
        	ctx.fill()
   

	surface.flush()
        surface.finish()
#
netdraw(net,512,512,outfile)
