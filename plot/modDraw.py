# Original Author: Dr. Tom Thorne
# Modified version of the draw module for new data and clustering results
# Modified by Paul Scherer
import math
import colorsys
import yaml
import cairo
import sys
import random

arg = sys.argv
infile = arg[1]
infileC = arg[2]
outfile = arg[3]
legendFile = arg[4]


# read the legendFile to make the associations.
# repopulate the legend
legend = {}
f = open(legendFile, 'r')
for line in f: 
	line.strip('\n')
	node, num = line.split()
	legend[node] = num
f.close()


# Loading of the files for parsing and extraction of the data points
# Reading of the yaml file and recreation of the net dictionary
f = open(infile, 'r')
net = yaml.load(f)
f.close()


# Population of the cluster list
cix = {}
for member in net['nodes'].keys(): 
	cix[member] = net['nodes'][member]['c']


# Population of the degree dictionary (size of the nodes based on the 
# degree of the node)
degs = {}
for node in net['nodes']:
	degs[node] = 0

for [a,b,w] in net['edges']:
	if a in degs: 
		degs[a] += 1
	else:
		degs[a] = 1
	if b in degs:
		degs[b] += 1
	else:
		degs[b] = 1

# # Delete nodes that havent been clustered
# edgesToDelete = []
# for clusNode in net['nodes'].keys():
# 	if net['nodes'][clusNode]['c'] == 1000:
# 		del net['nodes'][clusNode]

# 	for edgesTuple in net['edges']:
# 		a,b,w = edgesTuple
# 		if a == clusNode or b == clusNode:
# 			edgesToDelete.append(edgesTuple)

# for edgeD in edgesToDelete:
# 	#print edgeD
# 	try:
# 		net['edges'].remove(edgeD)
# 	except ValueError:
# 		continue

def netdraw(net, xs, ys, file):
	#clear
	surface = cairo.PDFSurface(file, xs, ys)
	
	# Set hyperparameter values for the context
	ctx = cairo.Context(surface)
	ctx.scale(xs, ys) # reset all coordinates 0.0->1.0
	ctx.rectangle(0,0,1,1)
	ctx.set_source_rgb(1.0,1.0,1.0)
	ctx.fill()
	ctx.set_line_join(cairo.LINE_JOIN_ROUND)

	# plotting of the edges
	for [a,b,w] in net['edges']:
			xa = 2.0*(float(net['nodes'][a]['x'])-0.5)+0.5
			ya = 2.0*(float(net['nodes'][a]['y'])-0.5)+0.5
			xb = 2.0*(float(net['nodes'][b]['x'])-0.5)+0.5
			yb = 2.0*(float(net['nodes'][b]['y'])-0.5)+0.5

			ctx.move_to(xa,ya)
			ctx.line_to(xb,yb)
			ctx.set_source_rgba(0.4,0.4,0.4,w)
			ctx.set_line_width(0.001)
			ctx.stroke()

	# color picker
	color = []
	for i in range(0,max(cix)):
		ohboy = random.random()
		color.append(ohboy)

	# nodes
	r = 0.001
	for i in net['nodes']:
		x = 2.0*(float(net['nodes'][i]['x'])-0.5)+0.5
		y = 2.0*(float(net['nodes'][i]['y'])-0.5)+0.5
		
		# setting of the node size (maybe want to change this)
		d = degs[i]
		if d>10:
			d = 10

		# outline
		# rgb = colorsys.hsv_to_rgb(0.7*(cix[i]-1)/max(cix),1.0,1.0)
		# rgbb = colorsys.hsv_to_rgb(0.7*(cix[i]-1)/max(cix),0.5,1.0)
		rgb = colorsys.hsv_to_rgb(color[cix[i]-1],1.0,1.0)
		rgbb = colorsys.hsv_to_rgb(color[cix[i]-1],0.5,1.0)
		# rgb = colorsys.hsv_to_rgb(0.7*(cix[i]-1)/10,1.0,1.0)
		# rgbb = colorsys.hsv_to_rgb(0.7*(cix[i]-1)/10,0.5,1.0)
		rr = r + d*0.0003

		# draw on the context
		ctx.arc(float(x),float(y), rr, 0, 2*math.pi)
		ctx.set_line_width(0.003)
		ctx.set_source_rgb(*rgb)
		ctx.stroke()
		ctx.arc(float(x), float(y), rr, 0, 2*math.pi)
		ctx.set_source_rgb(*rgbb)
		ctx.fill()

	surface.flush()
	surface.finish()

# draw the graph
netdraw(net, 512, 512, outfile)
