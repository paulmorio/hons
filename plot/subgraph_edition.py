# Original Author: Dr. Tom Thorne
# Modified to work with data and results of clusterings and parameters were changed as well
# This edition differs from the normal modBundle in that it only includes edges that are involved
# in clusters
# by Paul Scherer

# import necessary packages and libraries to perform work
import sys
import yaml
import math
import pyximport
pyximport.install()
import layout
import networkx as nx 

arg = sys.argv
infile = arg[1]		# Edge File
infileC = arg[2]	# Clusterings File
outfile = arg[3]	# Output yaml file
ck = float(arg[4])
legendFile = arg[5]	# Output Legend File

# Ready datastructure for storing graph
net = {}

# We start with the extraction of general metadata in our edges file
graph = nx.read_weighted_edgelist(infile)


# we take out the clustered nodes only
clusteredNodes = []
f = open(infileC, 'r')
for line in f:
	clusterMembers = line.strip('\n')
	clusterMembers = clusterMembers.split()

	for member in clusterMembers:
		clusteredNodes.append(member)

f.close()

subgraph = nx.subgraph(graph,clusteredNodes)
weights = nx.get_edge_attributes(subgraph, 'weight')

# populate the nodes dictionaries in net
# Edit: we change the names of proteins in net to integer id's 
net['nodes'] = {}
legend = {}
idAssign = 0
for x in subgraph.nodes():
	net['nodes'][idAssign] = {}
	legend[x] = idAssign
	idAssign += 1


"""
The large modification to the modBundle file is that
the clustering file is read first and clusters memberships
assigned to the nodes in the "net" dictionary.

When the cluster number is higher than the max cluster found
the edges of those nodes not involved are not to be included 
in the net dictionary
"""

# assign the cluster identities in the clusterfile
f = open(infileC, 'r')
clusterNumber = 1	# starting cluster assignment

for line in f:
	clusterMembers = line.strip('\n')
	clusterMembers = clusterMembers.split()

	for member in clusterMembers:
		member_num = legend[member]
		net['nodes'][member_num]['c'] = clusterNumber
		print str(member) + " " + str(member_num) + " " + str(net['nodes'][member_num]['c'])

	clusterNumber += 1

f.close()

# Cheap hack for points that were not cluster
# Edit: Delete member instead?
for member in net['nodes'].keys():
	if 'c' not in net['nodes'][member]:
		net['nodes'][member]['c'] = clusterNumber	#assign to own cluster for drawing
		clusterNumber += 1

# populate the edges dictionaries in net
f = open(infile, 'r')
ee = []
# for line in f:
# 	s=line.split()
# 	x=s[0]
# 	y=s[1]
# 	x = legend[x]	# assigning the name to number through the legend
# 	y = legend[y]
# 	print net['nodes'][x]['c']
# 	if (net['nodes'][x]['c'] < maxClusterNum or net['nodes'][x]['c'] < maxClusterNum):
# 		w=float(s[2])
# 		ee.append([x,y,math.pow(w/100,0.5)])

for edge in subgraph.edges():
	x,y = edge
	x = legend[x]	# assigning the name to number through the legend
	y = legend[y]
	w = float(weights[edge])
	ee.append([x,y,math.pow(w/100,0.5)])

print len(ee)
net['edges'] = ee
f.close()


# set layout and write to file
layout.layout(net,1.0/80,0.5,0.955,ck,100)
f = open(outfile,'w')
yaml.dump(net, f)
f.close()

# Writing down the legend
f = open(legendFile, 'w')
for x in legend.keys():
	f.write(str(x) + "\t" + str(legend[x]) + "\n")
f.close()

