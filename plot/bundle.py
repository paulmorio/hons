import sys
import yaml
import math
import pyximport
pyximport.install()
import layout

arg=sys.argv
infile=arg[1]
infileC=arg[2]
outfile=arg[3]
ck=float(arg[4])

f=open(infile,'r')
net = {}
nn = int(f.readline().split(',')[0])
net['nodes']={} 
for i in range(nn):
	net['nodes'][i]={}
ee=[]
for l in f:
    s=l.split(',')
    x=int(s[0])
    y=int(s[1])
    w=float(s[2])
    ee.append([x,y,math.pow(w,0.5)])
    #ee.append([x,y,w*10])
net['edges']=ee
f.close()
f=open(infileC,'r')
i=0
for l in f:
    net['nodes'][i]['c']=int(l)
    i=i+1
f.close()
layout.layout(net,1.0/80,0.5,0.955,ck,100)

f=open(outfile,'w')
yaml.dump(net,f)
f.close()
