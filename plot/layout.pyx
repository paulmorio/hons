import numpy as np
cimport numpy as np
import math
import random

import cython

cdef extern from "math.h":
        float sqrtf(float x)

@cython.boundscheck(False)
@cython.wraparound(False)

def layout(net,float K,float t,float cool,float ck,int steps):
        cdef np.int_t nn,i,j,x,y
        cdef np.float_t ax,ay,z,fx,fy
        nn=len(net['nodes'])
        cdef np.ndarray[np.float_t, ndim=1] px = np.zeros([nn], dtype=np.float)
        cdef np.ndarray[np.float_t, ndim=1] py = np.zeros([nn], dtype=np.float)
        cdef np.ndarray[np.float_t, ndim=1] a = np.zeros([nn*nn], dtype=np.float)
        cdef np.ndarray[np.int_t, ndim=1] c = np.zeros([nn], dtype=np.int)
        
        for i in xrange(nn):
                px[i]=random.random()
                py[i]=random.random()
                c[i]=net['nodes'][i]['c']

        for (x,y,w) in net['edges']:
                a[x+nn*y]=w
                a[y+nn*x]=w

        #force directed layout
        for step in xrange(steps):
                print step
                for i in xrange(nn):
                        fx=0.0
                        fy=0.0
                        for j in xrange(nn):
                                if i!=j:
                                        ax=px[i]-px[j]
                                        ay=py[i]-py[j]
                                        z=sqrtf(ax*ax+ay*ay)
                                        #attract
                                        if a[i+nn*j]>0:
                                                fx=fx-w*((z*z)/K)*(ax/z)
                                                fy=fy-w*((z*z)/K)*(ay/z)
                                        if c[i]==c[j]:
                                                fx=fx-ck*((z*z)/K)*(ax/z)*1000
                                                fy=fy-ck*((z*z)/K)*(ay/z)*1000
                                        #repulse
                                        fx=fx+((K*K)/z)*(ax/z)
                                        fy=fy+((K*K)/z)*(ay/z)
                        #edges
                        z=sqrtf((px[i]-0.5)*(px[i]-0.5)+(py[i]-0.5)*(py[i]-0.5))
                        ax=0.5-px[i]
                        ay=0.5-py[i]
                        ax=ax/z
                        ay=ay/z
                        fx=fx+((z*z)/K)*ax
                        fy=fy+((z*z)/K)*ay
                        #cool (annealing)
                        z=sqrtf(fx*fx+fy*fy)
                        if(z>t):
                                fx=t*fx/z
                                fy=t*fy/z
                        ax=px[i]+fx
                        if ax<1.0 and ax>0.0:
                                px[i]=ax
                        ay=py[i]+fy
                        if ay<1.0 and ay>0.0:
                                py[i]=ay
                t=t*cool
        for i in net['nodes']:
                net['nodes'][i]['x']=px[i]
                net['nodes'][i]['y']=py[i]


