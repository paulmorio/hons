import networkx as nx 
import matplotlib.pyplot as plt

# graph = nx.read_edgelist('/home/morio/workspace/hons/data/network_ICND_1_wo_weights.txt')
# print graph.number_of_nodes()
# print graph.number_of_edges()
graph = nx.erdos_renyi_graph(100, 0.15)
pos = nx.fruchterman_reingold_layout(graph)

nx.draw(graph, pos)

plt.show()