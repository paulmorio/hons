import networkx as nx
import random
import pylab as py

graph = nx.read_edgelist('/home/morio/workspace/hons/data/small')



# normal spring layout
nx.draw_random(graph, with_labels=False)
py.show()

# fixedpos = {}
# lineNumber = 0
# f = open('/home/morio/workspace/hons/data/thing', 'r')
# for clusterPos in f:
# 	clusterPos = clusterPos.strip('/n')
# 	clusterPos = clusterPos.split()
# 	clusterPos = clusterPos[0]
# 	fixedpos[clusterPos] = (lineNumber*100*(random.randint(-1,1)) , lineNumber*100*(random.randint(-1,1)))
# 	lineNumber += 1

# pos = nx.spring_layout(graph, fixed = fixedpos.keys(), pos = fixedpos)
# nx.draw_networkx(graph, pos=pos)

# py.show()