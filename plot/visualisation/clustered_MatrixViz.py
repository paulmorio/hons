"""
Unweighted Edition
This module visualises a yeast proteome as a matrix of binary interactions,
can be extended easily to incorporate the semantic similarity weightings as
a greyscaling of the visualisations.
"""

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np

graph = nx.read_edgelist('/home/morio/workspace/hons/data/smallunweighted')
mat_size = len(graph.nodes())

legend = {}
counter = 0
for i in graph.nodes():
	legend[i] = counter
	counter += 1

graphedges = graph.edges()

yeast_matrix = np.zeros((mat_size,mat_size))
for x,y in graphedges:
	print x,y
	x_mat = int(legend[x])
	y_mat = int(legend[y])
	yeast_matrix[x_mat,y_mat] += 1
	yeast_matrix[y_mat,x_mat] += 1

plt.matshow(yeast_matrix, cmap=plt.cm.gray)

plt.show()
